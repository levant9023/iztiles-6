<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>


<c:set var="req" value="${pageContext.request}" />
<c:set var='domain' value='${req.scheme}://${req.serverName}:${req.serverPort}'/>
<c:set var="baseURL" value="${domain}${req.contextPath}"/>
<c:set var="_lng" value="${requestScope.locale}"/>
<c:set var="lang" value="${fn:substring(_lng,0, 2)}"/>

<!DOCTYPE html>
<html lang="<c:out value="${lang}" default="en"/>" >
  <tiles:insertAttribute name="head"/>
  <body>
    <tiles:insertAttribute name="header"/>
    <div id="wrapper" class="wrapper">
      <tiles:insertAttribute name="content"/>
    </div>
    <tiles:insertAttribute name="footer"/>
  </body>
</html>


