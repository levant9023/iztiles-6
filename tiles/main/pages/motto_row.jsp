<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:set var="qe" value="false" />
<c:if test="${param.q ne ''}" >
	<c:set var="qe" value="true" />
</c:if>

<c:choose>
	<c:when test="${qe}">
		<%-- <a class="motto" href="/welcome.html" title="Search it easy!"><img src="${baseURL}/resources/img/search_easy.png" alt="Search it easy!"></a> --%>
	</c:when>
	<c:otherwise>
		<p align="center">Try search something like "san diego".</p>
	</c:otherwise>
</c:choose>
