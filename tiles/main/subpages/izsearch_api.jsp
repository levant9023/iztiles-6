<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>iZSearch search engine | iZSearch API</title>
    <link rel="shortcut icon" type="image/png" href="/resources/img/favicon.png">
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/api.css"/>
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/plugins/jsonview/jquery.json-viewer.css"/>
    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/api.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/plugins/jsonview/jquery.json-viewer.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/news_api.js"></script>
</head>
<body>
    <div id="header">
        <div id="logo">
            <a class="logo" href="/"><img src="${baseURL}/resources/img/subpages/news_api.png" alt="logo"></a>
        </div>
        <div id="account">
            <div id="started">
                <div class="hero-banner-btn">
                    <a id="topGetStartedForFree" href="#" target="_blank" class="btn--oxford btn--blue text--sm text--semilight">Get started for free</a>
                </div>
            </div>
        </div>
    </div>
    <div id="banner-container">
        <img src="/resources/img/API.png">
        <div id="banner-content">
            <h1 class="title-banner">iZSearch API</h1>
            <p class="text-banner">Add a variety of advanced search options to your app, web site or business.  Detailed insight into ~50 areas of human interest. Breaking, Trending and Popular news and other useful metadata within a single API call.</p>
        </div>
    </div>
    <div id="container">
        <div id="supbanner-text">
            <h2>Search for thematic news and get enhanced results</h2>
            <p>Search the web for news from ~50 areas of human interest. Search results provide useful metadata including authoritative image of the news article, full text of the article, brief snippet, URL, date,
                related news and categories and provider info. Try out the demo. Submit a query via the search box or click on one of the provided examples.</p>
        </div>
        <div id="select-menu">
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="link" >Go!</button>
                        </div>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
            <div class="accordion">
                <h1>Examples</h1>
                <div>
                    <ul id="cats">
                        <li href="javascript://"> San Diego</li>
                        <li href="javascript://">New York</li>
                        <li href="javascript://">Diabetes</li>
                        <li href="javascript://">Star Wars</li>
                        <li href="javascript://">Apple pie</li>
                        <li href="javascript://">Christmas</li>
                    </ul>
                </div>
            </div>
            <!--
            <div class="accordi on">
                <h1>Parameters</h1>
                <div class="collapse">
                    <div class="input-group param-row">
                        <span class="input-group-addon">Market</span>
                        <select type="text" class="param param-mkt form-control" data-qp="mkt" placeholder="Market">
                            <option value="de-de">de-de</option>
                            <option value="en-au">en-au</option>
                            <option value="en-ca">en-ca</option>
                            <option value="es-es">es-es</option>
                            <option value="en-gb">en-gb</option>
                            <option value="en-ie">en-ie</option>
                            <option value="en-in">en-in</option>
                            <option value="es-mx">es-mx</option>
                            <option value="en-nz">en-nz</option>
                            <option selected="" value="en-us">en-us</option>
                            <option value="fr-ca">fr-ca</option>
                            <option value="fr-fr">fr-fr</option>
                            <option value="it-it">it-it</option>
                            <option value="ja-jp">ja-jp</option>
                            <option value="nl-nl">nl-nl</option>
                            <option value="pt-br">pt-br</option>
                            <option value="zh-cn">zh-cn</option>
                        </select>
                    </div>
                    <div class="input-group param-row">
                        <span class="input-group-addon">Market</span>
                        <select type="text" class="param param-mkt form-control" data-qp="mkt" placeholder="Market">
                            <option value="de-de">de-de</option>
                            <option value="en-au">en-au</option>
                            <option value="en-ca">en-ca</option>
                            <option value="es-es">es-es</option>
                            <option value="en-gb">en-gb</option>
                            <option value="en-ie">en-ie</option>
                            <option value="en-in">en-in</option>
                        </select>
                    </div>
                </div>
                -->
                <script>
                    $('.accordion').accordion();
                </script>
            </div>
        </div>
        <div id="tab-container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#preview" aria-controls="home" role="tab" data-toggle="tab">Preview</a></li>
                <li role="presentation" data-name="json"><a href="#json" aria-controls="profile" role="tab" data-toggle="tab">JSON</a></li>
                <li role="presentation" data-name="tech"><a href="#tech" aria-controls="profile" role="tab" data-toggle="tab">Tech</a></li>
                <li role="presentation" data-name="business"><a href="#business" aria-controls="profile" role="tab"  data-toggle="tab">Business</a></li>
                <li role="presentation" data-name="sports"><a href="#sports" aria-controls="profile" role="tab" data-name="sports" data-toggle="tab">Sports</a></li>
                <li role="presentation" data-name="blogs"><a href="#blogs" aria-controls="profile" role="tab" data-toggle="tab">Blogs</a></li>
                <li role="presentation" data-name="travel"><a href="#travel" aria-controls="profile" role="tab" data-toggle="tab">Travel</a></li>
                <li role="presentation" data-name="food"><a href="#food" aria-controls="profile" role="tab" data-toggle="tab">Food</a></li>
                <!--<li role="presentation" data-name="health"><a href="#health" aria-controls="profile" role="tab" data-toggle="tab">health</a></li>
                <li role="presentation" data-name="science"><a href="#science" aria-controls="profile" role="tab" data-toggle="tab">science</a></li>
                <li role="presentation" data-name="celebrity"><a href="#celebrity" aria-controls="profile" role="tab" data-toggle="tab">celebrity</a></li>-->
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="preview"><div class="loader"></div></div>
                <div role="tabpanel" class="tab-pane" id="json"><div class="loader"></div></div>
                <div role="tabpanel" class="tab-pane" data-name="tech" id="tech"><div class="loader"></div></div>
                <div role="tabpanel" class="tab-pane" data-name="business" id="business"><div class="loader"></div></div>
                <div role="tabpanel" class="tab-pane" data-name="sports" id="sports"><div class="loader"></div></div>
                <div role="tabpanel" class="tab-pane" id="blogs"><div class="loader"></div></div>
                <!--<div role="tabpanel" class="tab-pane" id="sports"><div class="loader"></div></div>
                <div role="tabpanel" class="tab-pane" id="sports"><div class="loader"></div></div>-->
            </div>
        </div>
    </div>
    <div id="topics">
        <div class="topic-content">
            <h2>Thematic Search</h2>
            <p>Search thematic news by areas of human interest such as  General News, Technology, Business, Sports, Health, Shopping, Travel, Food, Science, Movies, Music and many others.</p>
        </div>
        <img src="${baseURL}/resources/img/api1.png">
    </div>
    <div id="topics2">
        <div class="topic-content">
            <h2>Breaking, Trending and Popular News by Topics</h2>
            <p>Get Breaking, Trending and Popular in Your area of interest, specify period of time (past month, past year, custom), language, geographic region, domain and other filters.</p>
        </div>
        <img src="${baseURL}/resources/img/api2.png">
    </div>
    <div id="price-table">
        <div class="pricing-section-content container">
            <h2 class="text--md text--centered text--light" style="margin-bottom:20px;clear:both;text-align: center;">Pricing options</h2>
            <table class="table apidetail-pricing-options">
                <tbody>
                <tr>
                    <th class="text--semibold">Plan</th>
                    <th class="text--semibold">Description</th>
                    <th class="text--semibold">Price</th>
                </tr>
                <tr>
                    <td class="text--semibold">Free</td>
                    <td class="text--reg">100 calls per month</td>
                    <td class="text--reg">Free</td>
                </tr>
                <tr>
                    <td class="text--semibold">Plan1</td>
                    <td class="text--reg">1K calls per month</td>
                    <td class="text--reg">$1 per month</td>
                </tr>
                <tr>
                    <td class="text--semibold">Plan2</td>
                    <td class="text--reg">10K calls per month</td>
                    <td class="text--reg">$10 per month</td>
                </tr>
                <tr>
                    <td class="text--semibold">Plan3</td>
                    <td class="text--reg">100K calls per month</td>
                    <td class="text--reg">$100 per month</td>
                </tr>
                <tr>
                    <td class="text--semibold">Plan4</td>
                    <td class="text--reg">500k calls per month</td>
                    <td class="text--reg">$500 per month</td>
                </tr>
                <tr>
                    <td class="text--semibold">Plan5</td>
                    <td class="text--reg">1M calls per month</td>
                    <td class="text--reg">$1,000 per month</td>
                </tr>
                <tr>
                    <td class="text--semibold">Plan6</td>
                    <td class="text--reg">5M calls per month</td>
                    <td class="text--reg">$5,000 per month</td>
                </tr>
                <tr>
                    <td class="text--semibold">Plan7</td>
                    <td class="text--reg">10M calls per month</td>
                    <td class="text--reg">$10,000 per month</td>
                </tr>
                <tr style="border:none">
                    <td colspan="3" style="border: none; padding: 0px !important">
                        <a href="#" id="btn-bottom" class="btn--oxford btn--blue col-xs-4 col-md-2 btn-buynow text--sm text--semilight">Buy on iZSearch<span class="buy-on-azure-img" style="margin-left:5px"></span></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <footer class="bottom-fix">
        <div class="col-md-12 text-center">
            <span style="font-size: 18px;">&#169 iZSearch. Search it easy!</span>
        </div>
    </footer>
</body>
</html>
