<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/about/styles/about-us.css"/>

<c:set var='title' value='less ads' scope="request"/>

<div class="container-fluid">
	<div id="board" class="row">
		<div id="logo" class="text-center">
			<a href="${baseURL}"><img src="${baseURL}/resources/img/logo.svg" alt="logo"></a>
			<div class="moto-logo">
				<h1>Have a Question? Check for answer on this page.</h1>
			</div>
		</div>
	</div>
	<div id="arts" class="row">
		<div id="lsa-text">
			<ul class="list-group lg-left">
				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/whatis.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is iZSearch?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is a general purpose search engine that is intended to be your starting place when searching the Internet. Use it to <b>get way more instant answers</b>, way less spam and real privacy, which we believe adds up to a much better overall search experience...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/izsearch-name.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Name?</h4>
						</div>
						<div class="qbody issue-body">
							<p>The name iZSearch is derived from Easy Search, but it's not a metaphor. If you're wondering how you would turn that into a verb... Search it easy!</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/agent-handle.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>How does iZSearch handle user agents?</h4>
						</div>
						<div class="qbody issue-body">
							<p>A user agent identifies your browser (Firefox, Internet Explorer, Safari, etc.) and provides certain system details to the websites you visit. Your web browser automatically sends this information to every webpage you visit...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/search-engine-in-chrome.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>How do I install iZSearch as my default search engine in Chrome?</h4>
						</div>
						<div class="qbody issue-body">
							<p>The Chrome web browser has a unique way of performing default searches. Rather than give you a search box in the upper right corner of your browser to perform searches, Chrome has you type your search term(s) directly into the address (URL) bar...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/what-is-ssl.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is a SSL/TLS Server Certificate?</h4>
						</div>
						<div class="qbody issue-body">
							<p>Whenever you use your web browser to communicate securely over the Internet (i.e. for banking, private search through iZSearch, etc.) you are using a technology known as SSL (Secure SocketLayer) or TLS (Transport Layer Security)...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/getting-cookies.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Am I still getting cookies even though I am using your service?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not use tracking cookies of any kind.</p>
							<p>There are two ways you could pick up a cookie when using our service:</p>
							<p>The first is an anonymous iZSearch Settings cookie that you can choose to accept...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/mylocation.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>How do you know my language or location if you don't store information about me?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not record IP addresses. We determine which browser you are using (Internet Explorer, Firefox, Safari, etc.) so we can customize the link on our home page that allows you to add iZSearch to your browser...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/cookie-policy.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is iZSearch's cookie policy?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not use tracking cookies, and we do not access or review cookies placed on your browser by other websites.</p>
							<p>If you use the iZSearch Settings page and save your preferences...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/server-logs.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What exactly gets recorded in your server logs? (IP Address? User Agent?)</h4>
						</div>
						<div class="qbody issue-body">
							<p>We have customized our Web server software so that it does not log any IP addresses or user agents. It writes 0.0.0.0 in lieu of an IP address and leaves a blank space for the user agent. No unique IP address is ever stored on our systems...</p>
						</div>
					</div>
				</li>
			</ul>

			<ul class="list-group lg-right">
				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/izsearch-architecture.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is Architecture of iZSearch?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is coded in Java, served via nginx, FastCGI and memcached, running on FreeBSD and Ubuntu via daemontools. We both run our own servers across the world. Your connection generally goes to the closest regional server available to your area...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/privacy-protection.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Does iZSearch add privacy protection to other sites I visit?</h4>
						</div>
						<div class="qbody issue-body">
							<p>No, iZSearch does not add privacy protection to websites you visit. The websites you visit are third-party websites and are not part of iZSearch. iZSearch protects your privacy as you search for information...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/windows-compatible.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Is iZSearch compatible with Windows 10 / Edge?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is now compatible with Windows 10/Edge.</p>
							<p>To add iZSearch as a default search engine in Edge, please open your Edge browser, and follow the instructions given here...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/collection-policy.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is your data collection policy? What do you record?</h4>
						</div>
						<div class="qbody issue-body">
							<p>We don't collect any personal information on our visitors.</p>
							<p>When you use iZSearch, we do not record your IP address, we do not record which browser you are using (Internet Explorer, Safari, Firefox, etc.), we do not record your...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/favorite-websites.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Can iZSearch store my favorite websites and bookmarks?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch makes a point of not storing your searches and not recording the websites you visit.</p>
							<p>However  iZSearch has a big library of popular resources that you can directly serach...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/extended-validation.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Does iZSearch have an Extended Validation (EV) SSL certificate?</h4>
						</div>
						<div class="qbody issue-body">
							<p>Traditional SSL certificates provide basic information about the certificate holder, as well as the organization that issued the certificate. However, the amount of validation done to confirm the identify of the organization requesting the certificate...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/encryption-protect.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>How does HTTPS encryption protect me? Does it keep my ISP from seeing me?</h4>
						</div>
						<div class="qbody issue-body">
							<p>The "https (SSL or "secure socket layer"/TLS "transport layer security") encrypted" connection you make with iZSearch prevents your Internet service provider (ISP) from seeing what you're searching for while you are on iZSearch...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/background-information.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Where can I find more background information on privacy issues?</h4>
						</div>
						<div class="qbody issue-body">
							<p>The websites of various privacy organizations are a great starting point for locating an abundance of background information on this subject...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Where is the site map?</h4>
						</div>
						<div class="qbody issue-body">
							<p>You can find sitemap by following this link: <a href="${baseURL}/subpages/sitemap.html">iZSearch sitemap</a></p>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
