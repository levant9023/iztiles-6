<%-- 
    Document   : open_community
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>


<c:set var='title' value='open community' scope="request"/>

<div class="container-fluid">  
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div> 
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>    
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h1>Open community</h1>
        <p>You can  help improve your search engine!</p>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <ul class="arts list-unstyled">
        <li>
          <h3>izSearch is a search engine driven by community</h3>
          <p>Interaction with our users community is through the Users Forum with features requests and immediate prioritizing by voting and further implemntation</p>
        </li>
      </ul>
    </div>
    <div class="col-md-2"></div>
  </div> 
</div>