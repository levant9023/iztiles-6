<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages.css" />
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/about/styles/features.css" />
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/plugins/flexislider/flexslider.css" />
<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/flexislider/jquery.flexslider-min.js"></script>

<c:set var="title" value="more features" scope="request"/>

<div class="container-fluid">
	<div id="board">
		<div id="logo">
			<div class="text-center">
				<a href="${baseURL}"><img src="${baseURL}/resources/img/logo.svg" alt="logo"/></a>
				<div class="moto-logo">
					<h1>iZSearch is Privacy focused, clean from the Ads and Spam web search engine.</h1>
				</div>
			</div>
		</div>
	</div>
	<div id="arts">
		<div class="art-wrapper">
			<ul class="arts list-unstyled">
				<li>
					<ul class="tour">
						<li class="tour-item">
							<div class="item-screenshot">
								<img src="${baseURL}/resources/img/features/mainwndow1.png" alt="image">
							</div>
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_01_48.png" alt="image"></span>
									<span class="big-title">Great Results</span>
								</div>
								<div class="item-text">
									<p class="descr">iZSearch has everything you expect a search engine to have, including images, videos, news and places, all while respecting your privacy.</p>
								</div>
							</div>
						</li>
						<li class="tour-item">
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_02_48.png" alt="image"></span>
									<span class="big-title">Search images, videos and places</span>
								</div>
								<div class="item-text">
									<p>iZSearch searches for images, videos, places and all kinds of media information.</p>
								</div>
							</div>
							<div class="item-screenshot flex">
								<ul class="slides">
									<li><img src="${baseURL}/resources/img/features/flex/images10.png" alt="image"></li>
									<li><img src="${baseURL}/resources/img/features/flex/videos10.png" alt="image"></li>
									<li><img src="${baseURL}/resources/img/features/flex/map.png" alt="image"></li>
								</ul>
							</div>
						</li>
						<li class="tour-item">
							<div class="item-screenshot">
								<img src="${baseURL}/resources/img/features/flex/mobile.png" alt="image">
							</div>
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_03_48.png" alt="image"></span>
									<span class="big-title">Mobile version</span>
								</div>
								<div class="item-text">
									<p class="descr">iZSearch automatically detects your mobile device (such as iphone, ipad, etc. ) and shows correct version of the site.</p>
								</div>
							</div>
						</li>
						<li id="vertical" class="tour-item">
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_04_48.png" alt="image"></span>
									<span class="big-title">Vertical (Thematic) search</span>
								</div>
								<div class="item-text">
									<p class="descr">Over 50 Popular verticals (Thematic pages) covered: Health, Food, Science, Tech, Music, Travel, Sports, Politics and many other.</p>
								</div>
							</div>
							<div class="item-screenshot flex">
								<ul class="slides">
									<li><img src="${baseURL}/resources/img/features/flex/vertical1.png" alt="image"></li>
									<li><img src="${baseURL}/resources/img/features/flex/vertical2.png" alt="image"></li>
									<li><img src="${baseURL}/resources/img/features/flex/vertical3.png" alt="image"></li>
								</ul>
							</div>
						</li>
						<li class="tour-item">
							<div class="item-screenshot">
								<img src="${baseURL}/resources/img/features/pageview1.png" alt="image">
							</div>
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_05_48.png" alt="image"></span>
									<span class="big-title">Page previews</span>
								</div>
								<div class="item-text">
									<p class="descr">Page previews allow you to qickly see the web page preview and check if the page is really what you want. Just roll over the title of the page. The feature that greatly saves your time!</p>
								</div>
							</div>
						</li>
						<li class="tour-item">
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_06_48.png" alt="image"></span>
									<span class="big-title">Quick Buttons</span>
								</div>
								<div class="item-text">
									<p class="descr">Quick Buttons help you search directly on thousands of other popular web sites. A search for shoes returns Quick Butons like Amazon, Zappos, Gap etc. that will take you right to a search for shoes on Amazon, Zappos, Gap etc.</p>
								</div>
							</div>
							<div class="item-screenshot">
								<img src="${baseURL}/resources/img/features/quickbuttons1.png" alt="image">
							</div>
						</li>
						<li class="tour-item">
							<div class="item-screenshot">
								<img src="${baseURL}/resources/img/features/topics1.png" alt="image">
							</div>
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_07_48.png" alt="image"></span>
									<span class="big-title">Topics</span>
								</div>
								<div class="item-text">
									<p class="descr">Topics - are thematic groups of visual buttons related to your search. For example for diabetes search you get Health, Health&Fitness and other themes, whereas for iphone 6s you get - Shopping, Gadgets, Tech reviews, News, Videos and other topics.</p>
								</div>
							</div>
						</li>
						<li class="tour-item">
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_08_48.png" alt="image"></span>
									<span class="big-title">Advanced search</span>
								</div>
								<div class="item-text">
									<p class="descr">Advanced search - allows you to search using search operators and other punctuation to get more specific search results. It allows you to narrow down your search results for complex searches. For example, you can “Find wikipedia pages talking about drugs affecting breast cancer on the gene or DNA level”: (drugs "breast cancer" (gene OR DNA) site: wikipedia.org) Additionally it allows you specify the types of results you to be returned (not only web pages), for example: Emails, Names, Cities, Countries, etc.</p>
								</div>
							</div>
							<div class="item-screenshot">
								<img src="${baseURL}/resources/img/features/advanced1.png" alt="image">
							</div>
						</li>
						<li class="tour-item">
							<div class="item-screenshot flex">
								<ul class="slides">
									<li><img src="${baseURL}/resources/img/features/bugs1.png" alt="image"></li>
									<li><img src="${baseURL}/resources/img/features/brep2.png" alt="image"></li>
								</ul>
							</div>
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_09_48.png" alt="image"></span>
									<span class="big-title">Interaction with User Community</span>
								</div>
								<div class="item-text">
									<p class="descr">You can report features, bugs or any problems live on the web site</p>
								</div>
							</div>
						</li>
						<li class="tour-item">
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_10_48.png" alt="image"></span>
									<span class="big-title">Real Privacy</span>
								</div>
								<div class="item-text">
									<p class="descr">We offer a search service which does not retain or share any of your personal information. iZSearch does not download “cookies” onto people’s devices. It does not register the “IP address” which pinpoints a users computer. It does not "filter" search results. That is distinct from what other companies do. We are not anonymizing/encrypting the data. We actually throw it away - everything related to the user and anything that is personally identifiable.</p>
								</div>
							</div>
							<div class="item-screenshot">
								<img src="${baseURL}/resources/img/features/privacy1.png" alt="image">
							</div>
						</li>
						<li id="ads" class="tour-item">
							<div class="item-screenshot">
								<img src="${baseURL}/resources/img/features/ads1.png" alt="image">
							</div>
							<div class="item-motto">
								<div class="item-title">
									<span class="big-cap"><img src="${baseURL}/resources/img/features/l_11_48.png" alt="image"></span>
									<span class="big-title">Fewer adds and commercials</span>
								</div>
								<div class="item-text">
									<p class="descr">And the last but not the least feature: we do really have much fewer Ads and clutter on the search results page then other search engines do. By default, iZSearch shows only minimal ads at the bottom of the search results page. iZSearch does not sell data about you to third parties, including advertisers and data brokers.</p>
								</div>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="col-md-2"></div>
	</div>
	<script type="text/javascript">
	$(document).ready(function () {
		var options = {
			direction: 'horizontal',
			slideshow: true,
			slideshowSpeed: 4000,
			animation: 'fade',
			animationSpeed: 1000,
			animationLoop: true,
			keyboard: false,
			controlNav: false,
			directionNav: false
		};
		$('.flex').flexslider(options);
	});
	</script>
</div>
