<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link type="text/css" rel="stylesheet" href="${baseURL}/resourcescss/keywords/chosen.css"/>
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/about/styles/about-us.css"/>
<c:set var="title" value="About" scope="request"/>
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/plugins/flexislider/flexslider.css"/>
<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/flexislider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/keywords/chosen.jquery.js"></script>

<jsp:useBean id="fields" class="java.util.ArrayList" scope="request"/>
<jsp:useBean id="words" class="java.util.ArrayList" scope="request"/>
<jsp:useBean id="keyForm" class="org.izsearch.model.forms.KeywordsForm" scope="request"/>
<c:set value="${requestScope.keywords}" var="issues"/>

<div class="container-fluid">
    <div class="header">
        <a class="logo" href="/"><img src="${baseURL}/resources/img/logo.svg" alt="logo"></a>
        <h1>Keywords Manager</h1>
    </div>

    <div class="container">
        <div class="art-wrapper">
            <form:form action="${baseURL}/keywords/add-user" method="post" modelAttribute="keyForm" enctype="multipart/form-data">
                <p><b>Email:</b>
                    <br>
                    <form:input path="email"/>
                    <br>
                <b>Password:</b>
                    <br>
                    <form:input path="pswd" type="password"/>
                </p>
                <%--<form action="${baseURL}/keywords.html" method="get">--%>
                <b>Keywords:</b>
                <br>
                <form:textarea path="keywords" cols="30" rows="8"></form:textarea>
                <br>
                <p><b>Title:</b>
                    <br>
                    <form:input path="title"/>
                    <br>
                    <b>Description:</b>
                    <br>
                    <form:input path="description"/>
                    <br>
                    <b>URL:</b>
                    <br>
                    <form:input path="url"/>
                </p>
                    <input type="submit" value="SEND">
            </form:form>
        </div>
    </div>
</div>

</div>
<script type="text/javascript">
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var options = {
            direction: 'horizontal',
            slideshow: true,
            slideshowSpeed: 4000,
            animation: 'fade',
            animationSpeed: 1000,
            animationLoop: true,
            keyboard: false,
            controlNav: false,
            directionNav: false
        };
        $('.flex').flexslider(options);
    });
    </
    style >
