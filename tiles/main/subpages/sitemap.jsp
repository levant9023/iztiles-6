<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/sitemap.css" />

<c:set var='title' value='more features' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <a href="${baseURL}"><img src="${baseURL}/resources/img/logo.svg" alt="logo"/></a>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Site Map</h2>
        <h1 class="grob">Site Map</h1>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="content" class="row">
    <div id="container" class="center">
      <ul id="columns">
        <li>
          <ul id="home-info" class="list-unstyled">
            <li class="title-item"><h3>Home</h3></li>
            <li class="item"><a href="${baseURL}/"><span>Main page</span></a></li>
            <li class="item"><a href="${baseURL}/images.html"><span>Images</span></a></li>
            <li class="item"><a href="${baseURL}/videos.html"><span>Videos</span></a></li>
            <li class="item"><a href="${baseURL}/news.html"><span>News</span></a></li>
            <li class="item"><a href="${baseURL}/blog.html"><span>Blogs</span></a></li>
            <li class="item"><a href="${baseURL}/forum.html"><span>Forums</span></a></li>
            <li class="item"><a href="${baseURL}/health.html"><span>Health</span></a></li>
            <li class="item"><a href="${baseURL}/food.html"><span>Food</span></a></li>
            <li class="item"><a href="${baseURL}/music.html"><span>Music</span></a></li>
            <li class="item"><a href="${baseURL}/entertainment.html"><span>Fun</span></a></li>
            <li class="item"><a href="${baseURL}/pets.html"><span>Pets</span></a></li>
            <li class="item"><a href="${baseURL}/science.html"><span>Science</span></a></li>
            <li class="item"><a href="${baseURL}/sports.html"><span>Sports</span></a></li>
            <li class="item"><a href="${baseURL}/tech.html"><span>Tech</span></a></li>
            <li class="item"><a href="${baseURL}/travel.html"><span>Travel</span></a></li>
          </ul>
        </li>

        <li>
          <ul id="map-info" class="list-unstyled">
            <li class="title-item"><h3>Information</h3></li>
            <li class="item"><a href="${baseURL}/private.html"><span>Privacy Policy</span></a></li>
            <li class="item"><a href="${baseURL}/terms.html"><span>Term and Conditions</span></a></li>
            <li class="item"><a href="${baseURL}/contacts.html"><span>Contacts</span></a></li>
            <li class="item"><a href="${baseURL}/about.html"><span>About us</span></a></li>
            <li class="item"><a href="${baseURL}/features.html"><span>Features</span></a></li>
            <li class="tail-item"><a href="${baseURL}/help.html"><span>Help</span></a></li>
          </ul>
        </li>

        <li>
          <ul id="a_q-info" class="list-unstyled">
            <li class="title-item"><h3>Questions and Answers</h3></li>
            <li class="item"><a href="${baseURL}/subpages/whatis.html"><span>What is iZSearch?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/izsearch-name.html"><span>iZSearch name?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/agent-handle.html"><span>How does iZSearch handle user
              agents?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/search-engine-in-chrome.html"><span>How do I install
              iZSearch as my default search engine in Chrome?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/what-is-ssl.html"><span>What is a SSL/TLS Server
              Certificate?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/getting-cookies.html"><span>Am I still getting cookies even
              though I am using your service?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/mylocation.html"><span>How do you know my language or location if you don't store information about me?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/cookie-policy.html"><span>What is iZSearch's cookie
              policy?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/server-logs.html"><span>What exactly gets recorded in your
              server logs? (IP Address? User Agent?)</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/izsearch-architecture.html"><span>What is Architecture of
              iZSearch?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/privacy-protection.html"><span>Does iZSearch add privacy
              protection to other sites I visit?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/windows-compatible.html"><span>Is iZSearch compatible with
              Windows 10 / Edge?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/collection_policy.html"><span>What is your data collection policy? What do you record?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/favorite-websites.html"><span>Can iZSearch store my
              favorite websites and bookmarks?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/extended-validation.html"><span>Does iZSearch have an
              Extended Validation (EV) SSL certificate?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/encryption-protect.html"><span>How does HTTPS encryption
              protect me? Does it keep my ISP from seeing me?</span></a></li>
            <li class="item"><a href="${baseURL}/subpages/background-information.html"><span>Where can I find more
              background information on privacy issues?</span></a></li>
          </ul>
        </li>

      </ul>

    </div>
  </div>

</div>