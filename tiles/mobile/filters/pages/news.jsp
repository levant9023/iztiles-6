<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="combined" value="${requestScope['snippets']['combined']}"/>
<c:set var="list1" value="${requestScope['snippets']['list1']}"/>
<c:set var="list2" value="${requestScope['snippets']['list2']}"/>
<c:set var="query" value="${requestScope['q']}"/>
<c:set var="rss" value="${requestScope['rss']}" />

<c:set var="nbf" value="false"/>
<c:choose>
	<c:when test="${(not empty requestScope.wpath) and (requestScope.wpath eq '/news.html')}">
		<c:set var="nbf" value="true"/>
	</c:when>
	<c:when test="${(not empty requestScope.wpath) and (requestScope.wpath eq '/blog.html')}">
		<c:set var="nbf" value="true"/>
	</c:when>
	<c:when test="${(not empty requestScope.wpath) and (requestScope.wpath eq '/forum.html')}">
		<c:set var="nbf" value="true"/>
	</c:when>
	<c:otherwise>
		<c:set var="nbf" value="false"/>
	</c:otherwise>
</c:choose>
