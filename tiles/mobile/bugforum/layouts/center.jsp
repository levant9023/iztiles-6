<%--
  User: Vlad L-I-S
  Date: 06.07.2016
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<c:set var="sort" value="${requestScope.sort}"/>

<c:choose>
    <c:when test="${sort eq '1'}">
        <c:set var='class_all' value='' />
        <c:set var='class_new' value='class="active"' />
        <c:set var='class_Inprogress' value='' />
        <c:set var='class_resolved' value='' />
    </c:when>
    <c:when test="${sort eq '2'}">
        <c:set var='class_all' value='' />
        <c:set var='class_new' value='' />
        <c:set var='class_Inprogress' value='class="active"' />
        <c:set var='class_resolved' value='' />
    </c:when>
    <c:when test="${sort eq '3'}">
        <c:set var='class_all' value='' />
        <c:set var='class_new' value='' />
        <c:set var='class_Inprogress' value='' />
        <c:set var='class_resolved' value='class="active"' />
    </c:when>
    <c:otherwise>
        <c:set var='class_all' value='class="active"' />
        <c:set var='class_new' value='' />
        <c:set var='class_Inprogress' value='' />
        <c:set var='class_resolved' value='' />
    </c:otherwise>
</c:choose>

<div id="center-wrapper" class="center_container container-fluid">
    <ul id="sorted" class="nav nav-tabs">
        <li role="presentation" ${class_all}><a href="${baseURL}/bugreport.html" class="lnkAll">All</a></li>
        <li role="presentation" ${class_new}><a href="${baseURL}/bugreport.html?sort=1" class="lnkNew">New</a></li>
        <li role="presentation" ${class_Inprogress}><a href="${baseURL}/bugreport.html?sort=2" class="lnkProgress">In progress</a></li>
        <li role="presentation" ${class_resolved}><a href="${baseURL}/bugreport.html?sort=3" class="lnkResolved">Resolved</a></li>
    </ul>
    <div id="search-text" class="content">
        <tiles:insertAttribute name="sections"/>
        <tiles:insertAttribute name="paginator"/>
    </div>
</div>