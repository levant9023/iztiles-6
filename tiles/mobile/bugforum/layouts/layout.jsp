<%--
  User: Vlad L-I-S
  Date: 6/7/16
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<!DOCTYPE HTML>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="meta_title" value="${requestScope.meta_title}" scope="request"/>

<html lang="<c:out value="${lang}"/>">
<head>
    <base href="${baseURL}/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results.">
    <meta name="keywords" content="izsearch, izsearch home, izsearch home page, izsearch search, izsearch web, news, blog, forum, health, sports, travel, tech, entertainment, video, image">
    <title>iZSearch:&nbsp;Bug Report</title>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/css/owl.carousel.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/css/owl.theme.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/mobile/search.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/justifiedGallery/styles.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/moment-locales.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/can.custom.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/modules/owl.carousel.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/mvc/classes.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/mvc/icons.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/modules/js.cookie.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/plugins/justifiedGallery/script.min.js"></script>
    <script type="text/javascript">
        var lang = "<c:out value='${lang}' default='en'/>";
    </script>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-58665066-1', 'auto');
        ga('require', 'linkid', 'linkid.js');
        ga('send', 'pageview');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter31760636 = new Ya.Metrika({
                        id:31760636,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        trackHash:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/31760636" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>

<div id="wrapper" class="container-fluid">
    <tiles:insertAttribute name="header"/>
    <div id="rsads-wrapper" class="row hide">
        <div class="popover-content">
            <div id="redmine-report">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="bugfix-title">Report bugs</h3>
                    </div>
                    <div class="panel-body">
                        <form id="form-redmine" class="izwidget" role="form">
                            <div class="form-group">
                                <label for="inputUserName" class="control-label">Your name (required)*:</label>
                                <input type="text" class="form-control" id="inputUserName" name="username" value="">
                                <span id="username_message" class="message"></span>
                            </div>
                            <div class="form-group">
                                <label for="inputBugEmail" class="control-label">Your email (required)*:</label>
                                <input type="email" class="form-control" id="inputBugEmail" name="usermail">
                                <span id="usermail_message" class="message"></span>
                            </div>
                            <div class="form-group">
                                <label for="inputBugSubject" class="control-label">Subject (required) *:</label>
                                <input type="text" class="form-control" id="inputBugSubject" name="bugsubject">
                                <span id="bugsubject_message" class="message"></span>
                            </div>
                            <div class="form-group">
                                <label for="inputBugDescr" class="control-label">Bug description (required)*:</label>
                                <textarea class="form-control" rows="2" id="inputBugDescr" name="bugdescr"></textarea>
                                <span id="bugdescr_message" class="message"></span>
                            </div>
                            <div class="form-group">
                                <div class="btn-send">
                                    <span id="redmine-result-message" class="message"></span>
                                    <button id="btn-redmine" type="button" class="btn btn-primary btn-block">
                                        Send
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="middle" class="row">
        <tiles:insertAttribute name="center"/>
    </section>
    <tiles:insertAttribute name="footer"/>
</div>
</body>
</html>
