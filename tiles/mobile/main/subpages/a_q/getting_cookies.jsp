<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo_search.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">Am I still getting cookies even though I am using your service?</h1>
      <h3>Am I still getting cookies even though I am using your service?</h3>
      <p>iZSearch does not use tracking cookies of any kind.</p>
      <p>There are two ways you could pick up a cookie when using our service:</p>
      <p>The first is an anonymous iZSearch Settings cookie that you can choose to accept on our Settings page. This is a privacy-friendly cookie that remembers your preferences each time you return to iZSearch. It does not uniquely identify you and it is never used for tracking. (Note that if you choose not to install the Settings cookie, you can still save your settings with the URL generator at the bottom of the Settings page instead.)</p>
      <p>The second way you could be picking up cookies is from third-party websites you visit through iZSearch. This occurs when you leave iZSearch by clicking on a website result. Here's how it works:</p>
      <p>Whenever you search with iZSearch, your SEARCH is not recorded, your IP ADDRESS is not recorded, your IDENTITY is not recorded, no TRACKING COOKIES are placed on your browser, and our SSL/TLS ENCRYPTION ensures that your ISP or hackers can't eavesdrop. You are never seen by any of the search engines, including Google.</p>
      <p>That protection applies 100% to your SEARCHES. However, when you CLICK on a search result, you LEAVE the search engine that gave you the link and go SOMEWHERE ELSE.</p>
      <p>Wherever you go, once you leave <b>iZSearch</b>, you can be seen, recorded, and tracked by the website you are going to, plus by all of its advertising and marketing and tracking partners and affiliates.</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
