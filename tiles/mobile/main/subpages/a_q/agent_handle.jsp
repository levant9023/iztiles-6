<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo_search.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">How does iZSearch handle user agents?</h1>
      <h3>How does iZSearch handle user agents?</h3>

      <p>A user agent identifies your browser (Firefox, Internet Explorer, Safari, etc.) and provides certain
        system details to the websites you visit. Your web browser automatically sends this information to every
        webpage you visit. The user agent data helps websites tailor their pages and content to your browser and
        platform. You can refer to Wikipedia and Microsoft for additional information on user agents here:<br>
        <a href="https://en.wikipedia.org/wiki/User_agent">https://en.wikipedia.org/wiki/User_agent</a> <br>
        <a href="http://msdn.microsoft.com/en-us/library/ms537503(v=vs.85).aspx">http://msdn.microsoft.com/en-us/library/ms537503(v=vs.85).aspx</a>
        iZSearch <b>does not store your user agent data</b>. That is very important, because user agent data can
        be used to "fingerprint" your browser and identify you. We never collect or store add-on details either,
        as they are specific enough to make browser fingerprinting possible. The only information we log is
        total search numbers conducted per language, as an aggregate figure.</p>

      <p>iZSearch does not store your user agent data. That is very important, because user agent data can be
        used to "fingerprint" your browser and identify you. We never collect or store add-on details either, as
        they are specific enough to make browser fingerprinting possible. The only information we log is total
        search numbers conducted per language, as an aggregate figure.</p>

      <p>iZSearch uses your user agent as follows: To customize the "Add to... " message on our home page.
        iZSearch uses the user agent string to customize the home page(www.iZSearch.com) where it provides
        instructions to add iZSearch to your browser. (That's under the search box where it says "Add to
        Internet Explorer," "Add to Firefox," "Add to Safari," etc.) Those directions differ from browser to
        browser, so we tailor the instructions you see.</p>

      <p>User agent data helps us prevent our service from being "scraped" by automatic programs or bots. One
        way we can identify bots is that they often do not send user-agents, while a normal user of our service
        would. This is why we do not allow access to iZSearch by visitors who do not transmit a user agent.</p>

      <p>A number of browsers also offer add-ons so that you can "fake" your user agent data thereby enhancing
        your privacy and overcoming the perceived scraping obstacle.</p>

      <p>A final word about fingerprinting techniques: iZSearch does not attempt to "fingerprint" and track
        visitors, under any circumstances. iZSearch opposes the use of so-called "canvas fingerprinting" and
        evercookies, and does not use these or any other techniques to gather and retain data about you, your
        computing environment, or your browsing habits. If you are concerned about the use of canvas
        fingerprinting or evercookies, our Proxy can protect you from both when browsing web sites.</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
