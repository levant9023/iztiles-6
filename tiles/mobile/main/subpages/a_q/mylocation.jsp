<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo_search.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h3 class="grob">How do you know my language or location if you don't store information about me?</h3>
     <h3>How do you know my language or location if you don't store information about me?</h3>
      <p>iZSearch does not record IP addresses.
        We determine which browser you are using (Internet Explorer, Firefox, Safari, etc.) so we can customize the link on our home page that allows you to add iZSearch to your browser. Since your browser configuration also tells us, anonymously, what language your browser is set to, that allows us to seamlessly deliver content in the language you are using. In addition to the search term, we use an anonymized country code that is required to provide results at the country level for your search. In no circumstance do we share your actual IP address, exact location, or other personally identifiable data.</p>
      <p>We do not log or make any permanent record of which browser you are using or which language your browser is set to, since we keep no record of your presence at all.</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
