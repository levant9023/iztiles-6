<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo_search.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">How do I install iZSearch as my default search engine in Chrome?</h1>
      <h3>How do I install iZSearch as my default search engine in Chrome?</h3>
      <p>The Chrome web browser has a unique way of performing default searches. Rather than give you a search
        box in the upper right corner of your browser to perform searches, Chrome has you type your search
        term(s) directly into the address (URL) bar.</p>

      <p>To set iZSearch as your Chrome browser default search engine:</p>
      <ol>
        <li>Click on the three horizontal bars at the top right in your Chrome browser and select Settings.</li>
        <li>Now locate the Search area and click Manage search engines ...</li>
        <li>In the popup, scroll to the bottom of the list of Other search engines, and fill out the three
          fields:
          <ul>
            <li>Add a new search engine: iZSearch</li>
            <li>Keyword: iZSearch.com</li>
            <li>URL with %s in place of query: <a
                href="https://iZSearch.com/do/search?query=%s&cat=web&pl=chrome&language=english">https://iZSearch.com/do/search?query=%s</a>
            </li>
          </ul>
        </li>
        <li>When you are finished, click Done and the window will close.</li>
        <li>Click on Manage search engines... again (see Step 2 above), locate iZSearch under the list of Other
          search engines, place your mouse over the iZSearch entry in the list and click Make default.
        </li>
        <li>When you are finished, click Done and close the the Settings tab.</li>
      </ol>
      <p><b>Note:</b> If you cannot find the above options in your Chrome browser, please make sure that your
        Chrome version is up to date.</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
