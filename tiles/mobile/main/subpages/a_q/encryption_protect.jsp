<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo_search.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">How does HTTPS encryption protect me? Does it keep my ISP from seeing me?</h1>
      <h3>How does HTTPS encryption protect me? Does it keep my ISP from seeing me?</h3>
      <p>The "https (SSL or "secure socket layer"/TLS "transport layer security") encrypted" connection you make with iZSearch prevents your Internet service provider (ISP) from seeing what you're searching for while you are on iZSearch.</p>
      <p>For maximum privacy, here are a few tips:
        You should understand that whenever you search with iZSearch, your SEARCH is not recorded, your IP ADDRESS is not recorded, your IDENTITY is not recorded, no TRACKING COOKIES are placed on your browser, and our "HTTPS (SSL/TLS) ENCRYPTION" ensures that your ISP or hackers can't eavesdrop on what you search for or see through iZSearch. You are also never seen by any of the search engines (e.g. Google).</p>
      <p>That protection applies 100% to your SEARCHES. However, when you CLICK on a search result, including sponsored results, you LEAVE the search engine that gave you the link and go SOMEWHERE ELSE.</p>
      <p>Once you leave iZSearch, you can be seen, recorded, and tracked by the website you are going to, plus by all of its advertising and marketing and tracking partners and affiliates.</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
