<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>API</title>
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/mobile/api.css"/>
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/plugins/highlight/default.css"/>
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/plugins/jsonview/jquery.json-viewer.css"/>
    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/api.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/plugins/jsonview/jquery.json-viewer.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/news_api.js"></script>
</head>
<body>
<div id="header">
    <div id="logo">
        <a class="logo" href="/"><img src="${baseURL}/resources/img/subpages/news_api.png" alt="logo"></a>
    </div>
</div>
<div id="banner-container">
    <div id="banner-content">
        <h1 class="title-banner">iZSearch Thematic News API</h1>
        <p class="text-banner">Add a variety of advanced thematic news search options to your app, web site or business.  Detailed insight into thematic news from over 40 areas of human interest. Get Breaking, Trending and Popular news and other useful metadata with a single API call.</p>
        <div id="started">
            <div class="hero-banner-btn">
                <a id="topGetStartedForFree" href="#" target="_blank" class="btn--oxford btn--blue text--sm text--semilight">Get started for free</a>
            </div>
        </div>
    </div>
</div>
<div id="container">
    <div id="supbanner-text">
        <h2> Search for thematic news and get enhanced results</h2>
        <p>Search the web for news in thematic categories from over 40 areas of human interest. Results provide useful metadata including authoritative image of the news article, related news and categories, provider info, article URL, and date added. Try out the demo. Submit a query via the search box or click on one of the provided examples.</p>
    </div>
    <div id="select-menu">
        <div class="row">
            <div class="col-lg-6">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="button" id="link" >Go!</button>
                    </div>
                </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="accordion">
            <h1>Examples</h1>
            <div>
                <ul id="cats">
                    <li href="javascript://"> San Diego</li>
                    <li href="javascript://">New York</li>
                    <li href="javascript://">Diabetes</li>
                    <li href="javascript://">Star Wars</li>
                    <li href="javascript://">Apple pie</li>
                    <li href="javascript://">Christmas</li>
                </ul>
            </div>
        </div>
        <!--
        <div class="accordion">
            <h1>Parameters</h1>
            <div class="collapse">
                <div class="input-group param-row">
                    <span class="input-group-addon">Market</span>
                    <select type="text" class="param param-mkt form-control" data-qp="mkt" placeholder="Market">
                        <option value="de-de">de-de</option>
                        <option value="en-au">en-au</option>
                        <option value="en-ca">en-ca</option>
                        <option value="es-es">es-es</option>
                        <option value="en-gb">en-gb</option>
                        <option value="en-ie">en-ie</option>
                        <option value="en-in">en-in</option>
                        <option value="es-mx">es-mx</option>
                        <option value="en-nz">en-nz</option>
                        <option selected="" value="en-us">en-us</option>
                        <option value="fr-ca">fr-ca</option>
                        <option value="fr-fr">fr-fr</option>
                        <option value="it-it">it-it</option>
                        <option value="ja-jp">ja-jp</option>
                        <option value="nl-nl">nl-nl</option>
                        <option value="pt-br">pt-br</option>
                        <option value="zh-cn">zh-cn</option>
                    </select>
                </div>
                <div class="input-group param-row">
                    <span class="input-group-addon">Market</span>
                    <select type="text" class="param param-mkt form-control" data-qp="mkt" placeholder="Market">
                        <option value="de-de">de-de</option>
                        <option value="en-au">en-au</option>
                        <option value="en-ca">en-ca</option>
                        <option value="es-es">es-es</option>
                        <option value="en-gb">en-gb</option>
                        <option value="en-ie">en-ie</option>
                        <option value="en-in">en-in</option>
                    </select>
                </div>
            </div>
            -->
            <script>
                $('.accordion').accordion();
            </script>
        </div>
    </div>
    <div id="tab-container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#preview" aria-controls="home" role="tab" data-toggle="tab">Preview</a></li>
            <li role="presentation"><a href="#json" aria-controls="profile" role="tab" data-toggle="tab">JSON</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="preview"><div class="loader"></div></div>
            <div role="tabpanel" class="tab-pane" id="json"><div class="loader"></div></div>
        </div>
    </div>
</div>
<div id="topics">
    <div class="topic-content">
        <h2>Thematic Search</h2>
        <p>Thematic news search by areas of human interest such as  General News, Technology, Business, Sports, Health, Shopping, Travel, Food, Science, Movies, Music and many others.</p>
    </div>
</div>
<div id="topics2">
    <div class="topic-content">
        <h2>Breaking, Trending and Popular News by Topics</h2>
        <p>Customize your news of interest by Breaking, Trending and Popular over some period in past (past month, past year, etc.) by area of interest, language and geographic region.</p>
    </div>
</div>
<div id="price-table">
    <div class="pricing-section-content container">
        <h2 class="text--md text--centered text--light" style="margin-bottom:20px;clear:both;text-align: center;">Pricing options</h2>
        <table class="table apidetail-pricing-options">
            <tbody>
            <tr>
                <th class="text--semibold">Plan</th>
                <th class="text--semibold">Description</th>
                <th class="text--semibold">Price</th>
            </tr>
            <tr>
                <td class="text--semibold">Free</td>
                <td class="text--reg">100 calls per month</td>
                <td class="text--reg">Free</td>
            </tr>
            <tr>
                <td class="text--semibold">Plan1</td>
                <td class="text--reg">1K calls per month</td>
                <td class="text--reg">$1 per month</td>
            </tr>
            <tr>
                <td class="text--semibold">Plan2</td>
                <td class="text--reg">10K calls per month</td>
                <td class="text--reg">$10 per month</td>
            </tr>
            <tr>
                <td class="text--semibold">Plan3</td>
                <td class="text--reg">100K calls per month</td>
                <td class="text--reg">$100 per month</td>
            </tr>
            <tr>
                <td class="text--semibold">Plan4</td>
                <td class="text--reg">500k calls per month</td>
                <td class="text--reg">$500 per month</td>
            </tr>
            <tr>
                <td class="text--semibold">Plan5</td>
                <td class="text--reg">1M calls per month</td>
                <td class="text--reg">$1,000 per month</td>
            </tr>
            <tr>
                <td class="text--semibold">Plan6</td>
                <td class="text--reg">5M calls per month</td>
                <td class="text--reg">$5,000 per month</td>
            </tr>
            <tr>
                <td class="text--semibold">Plan7</td>
                <td class="text--reg">10M calls per month</td>
                <td class="text--reg">$10,000 per month</td>
            </tr>
            </tbody>
        </table>
        <div id="buy_btn">
            <a href="#" id="btn-bottom" class="btn--oxford btn--blue col-xs-4 col-md-2 btn-buynow text--sm text--semilight">Buy on iZSearch<span class="buy-on-azure-img" style="margin-left:5px"></span></a>
        </div>
    </div>
</div>
<footer class="bottom-fix">
    <div class="col-md-12 text-center">
        <span style="font-size: 18px;">&#169 iZSearch. Search it easy!</span>
    </div>
</footer>
</body>
</html>
