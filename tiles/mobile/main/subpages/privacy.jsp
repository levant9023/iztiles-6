<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var="title" value="private search" scope="request" />

<div class="container-fluid">
	<div class="header text-center">
		<a class="logo" href="/"><img src="${baseURL}/resources/img/logo.svg" alt="logo"></a>
		<h1>Privacy Policy</h1>
	</div>

	<div class="container">
		<h3>Personal Information</h3>
		<p>iZSearch does not retain or share personal information with any third party, including its provider(s) or sponsored search results.</p>
		<p>Some results page may include third party products or services such a small number of sponsored links to generate revenue and cover operational costs. Those links are retrieved from reputed platforms such as Google AdSense, eBay and others. However, all these links are safe for the user, because they do not transmit any personal information to the site owners. Furthermore, all of these links are processed first by engine iZSearch, which first clear it and after proxy to сorresponding resources.</p>

		<h3>IP Addresses</h3>
		<p>The IP address that is associated with your search will not be ever saved or shared.</p>

		<h3>Cookies</h3>
		<p>Cookies is a small piece of data sent from a website and stored in your web browser (computer's hard drive) when you visiting website.</p>
		<p>iZSearch use a small count of cookies that are only necessary for correctly working a webpage GUI widgets.</p>
		<p>You can choose to accept or refuse cookies by changing the settings of your browser. However you should be aware that this action can afect properly functioning of some javascript elements on the webpage.</p>
		<p>Session cookies expire when you close the browser. Persistent such (flash, images etc.) have typical expiration dates ranging from two months up to a couple of years.</p>
	</div>
</div>
