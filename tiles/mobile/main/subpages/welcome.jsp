<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<c:set var="title" value="welcome" scope="request"/>

<div class="container-fluid">
	<div class="header text-center">
		<a class="logo" href="${baseURL}"><img src="${baseURL}/resources/img/logo.svg" alt="logo"></a>
		<h1>No Ads. No Spam. No Tracking.</h1>
	</div>

	<div class="videos row">
		<div class="col-sm-4">
			<iframe src="https://www.youtube.com/embed/ScGkfzE1X38" frameborder="0" allowfullscreen></iframe>
			<p>Can you imagine search engine without heavy and distracting ads, without spam and tracking? Try new clean search engine - iZSearch</p>
		</div>
		<div class="col-sm-4">
			<iframe src="https://www.youtube.com/embed/GYElhlwhk5M" frameborder="0" allowfullscreen></iframe>
			<p>iZSearch.com shows only minimal ads at the bottom of the search results page</p>
		</div>
		<div class="col-sm-4">
			<iframe src="https://www.youtube.com/embed/-nple-4M80s" frameborder="0" allowfullscreen></iframe>
			<p>iZSearch does not retain or share any of your personal information.</p>
		</div>
		<div class="col-sm-4">
			<iframe src="https://www.youtube.com/embed/TBEAo5KTLr0" frameborder="0" allowfullscreen></iframe>
			<p>iZSearch does not promote its own services like social networks (e.g. Google+), mail (e.g. Gmail) or other, but rather searches for the public services and pages that are most relevant for your query.</p>
		</div>
		<div class="col-sm-4">
			<iframe src="https://www.youtube.com/embed/gD6ftip3C98" frameborder="0" allowfullscreen></iframe>
			<p>Over 50 thematic search pages for practically any area of human interest (from Sports and Science to Children and Seniors) are available on iZSearch.com.</p>
		</div>
	</div>
</div>
