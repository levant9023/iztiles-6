<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var="title" value="iZSearch Apps" scope="request" />

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/mobile/subpages/about/styles/about-us.css" />
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/plugins/flexislider/flexslider.css" />
<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/flexislider/jquery.flexslider-min.js"></script>
		<div class="header">
			<a class="logo" href="${baseURL}"><img src="${baseURL}/resources/img/logo.svg" alt="logo"></a>
			<h1><span style="font-size:30px;font-weight:bold;color:#fff;margin-right:10px;">iZebra<sup>&#169</sup></span>Android app</h1>
			<div id="top-app">
				<div id="top-text">
				<div id="top-app-title">
					<p>iZebra<sup>&#169 </sup>is the smart Android app to search, read and store categorized news</p>
				</div>
				<div id="top-app-txt">
					<p class="ttxt"> Read what you're interested in.</p>
					<p>iZebra<sup>&#169 </sup>app is the fast and efficient way to get all your news now. It feeds you with the news on about 40 various domains and areas of interest. You can specify and create your own area of interest with specific keywords. Zero loading time, a beautiful interface!</p>
					<br>
					<p>Zebra finds news important for you and saves time.</p>
				</div>
			</div>
			<div id="slider-flex">
				<div id="app-flex" class="flexslider">
					<ul class="slides">
						<li><img src="${baseURL}/resources/img/about/app/arm1.png" alt="image"></li>
						<li><img src="${baseURL}/resources/img/about/app/arm2.png" alt="image"></li>
						<li><img src="${baseURL}/resources/img/about/app/arm3.png" alt="image"></li>
						<li><img src="${baseURL}/resources/img/about/app/arm4.png" alt="image"></li>
					</ul>
				</div>
			</div>
            </div>
            <div id="download-elements">
            <div class="download-elements">
                    <img src="/resources/img/about/app/app_ad.png">
            </div>
            <div class="download-elements">
                <a href="/resources/downloads/izebra.apk" title="Click to download iZebra android .apk package"></a>
                <button type="button" class="btn btn-primary btn-lg" style="margin-bottom: 80px; margin-top: 40px;" href="/downloads/iZebra.apk">DOWNLOAD</button>
            </div>
            </div>
	</div>
	<div class="container">
			<br class="art-wrapper">
				<h2 align="center" style="font-size:32px;">Always be aware of the latest events in the world, on all your news categories.</h2>
				<div id="box-col">
					<div class="img-col">
						<img src="/resources/img/about/app/01.png">
					</div>
					<div class="block_title"><span >Search news in over 40 areas of interest</span></div>
					<p>Currently we have 40 popular categories for practically any area of human interest: News, Blogs, Technology, Business, Politics, Health, Food, Entertainment, Music, Travel, Pets, and many other. No need to read a large amount of useless information. Just select the category that you are interested in and search news only in specific topics.</p>
                </div>
				<div id="box-col2">
					<div class="img-col">
						<img src="/resources/img/about/app/02.png">
					</div>
					<div class="block_title"><span>Custom categories</span></div>
                    <p>Create your own category to collect the news that are of interest only to you, any topic, any content. Add, delete, change the theme that you want to see in their own category. Total freedom of action - complete convenience. Also, your category is completely private. Topics that interest you will be known only to you.
                    </p>
                    </p>
				</div>
				<div class="horizontal-box">
					<img src="/resources/img/about/app/03.png">
					<div id="horiz-title">
						<span>Add to favorites</span>
					<p> Sometimes there are topics of interest that you want to use again and again. iZebra has a feature that allows you to store content in your “Favorites” to be easily accessed. Just select the content you want to save, click Favorite and your content will be saved.</p>
					</div>
					<div class="horizontal-box">
						<img src="/resources/img/about/app/04.png">
						<div id="horiz-title2">
							<span>Share on social networks</span>
							<p>Easily share any link to Facebook, Twitter and Google+. Click the "Share" button and share this news to your own timeline, or a friend's, to a group or Page, or in private message.</p>
						</div>
					</div>


		</div>

<script type="text/javascript">
	$(document).ready(function () {
		var options = {
			direction: 'horizontal',
			slideshow: true,
			slideshowSpeed: 4000,
			animation: 'fade',
			animationSpeed: 1000,
			animationLoop: true,
			keyboard: false,
			controlNav: false,
			directionNav: false
		};
		$('#app-flex').flexslider(options);
	});
</script>
