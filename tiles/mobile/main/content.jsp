<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>



<c:set var="count" value="10" scope="page"/>

<c:url var="bagreport_url" value="/bugreport.html?page=1"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="main_url" value="${baseURL}"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="images_url" value="/images.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="videos_url" value="/video.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="map_url" value="/map.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="news_url" value="/news.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="forums_url" value="/forum.html"><c:param name="q" value="${param.q}"/></c:url>

<c:url var="art_url" value="/art.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="blogs_magazines_url" value="/blogs_magazines.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="books_url" value="/books.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="business_url" value="/business.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="cars_url" value="/cars.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="celebrity_url" value="/celebrity.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="craft_url" value="/craft.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="design_url" value="/design.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="education_url" value="/education.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="family_url" value="/family.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="food_recipes_url" value="/food_recipes.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="fun_url" value="/fun.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="gifts_url" value="/gifts.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="health_url" value="/health.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="home_url" value="/home.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="jobs_url" value="/jobs.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="industrial_url" value="/industrial.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="kids_url" value="/kids.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="law_url" value="/law.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="lifestyle_url" value="/lifestyle.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="men_url" value="/men.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="movies_url" value="/movies.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="music_url" value="/music.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="npo_url" value="/npo.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="pets_url" value="/pets.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="politics_url" value="/politics.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="real_estate_url" value="/real_estate.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="science_url" value="/science.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="seniors_url" value="/seniors.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="shopping_url" value="/shopping.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="sports_url" value="/sports.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="style_fashion_url" value="/style_fashion.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="teachers_url" value="/teachers.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="tech_url" value="/tech.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="teens_url" value="/teens.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="tips_tutorials_url" value="/tips_tutorials.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="travel_url" value="/travel.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="tools_url" value="/tools.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="tv_url" value="/tv.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="women_url" value="/women.html"><c:param name="q" value="${param.q}"/></c:url>

<div id="info" class="addBookmark addto">
	<i id="show_info" class="glyphicon glyphicon-option-vertical"></i>
	<div class="container clearfix">
		<div class="sidebar">
			<i id="show_info_close" class="glyphicon glyphicon-option-vertical"></i>
			<ul class="nav">
				<li><a href="${baseURL}/about.html"><i class="glyphicon glyphicon-info-sign"></i>About</a></li>
				<li><a href="http://blog.izsearch.com"><i class="glyphicon glyphicon-pencil"></i>Blog</a></li>
				<li><a href="${baseURL}/api.html"><i class="btn-suscr glyphicon glyphicon-bullhorn" style="font-size:21px;"></i>API</a></li>
				<li><a id="brand-api" href="${baseURL}/brand/intro.html"><i class="glyphicon glyphicon-random"></i>iZ Brands</a></li>
				<li><a id="bbtn-advertise" href="${baseURL}/advertise.html"><i class="glyphicon glyphicon-tags"></i>iZ Ads</a></li>
				<li><a id="bbtn-investor" href="${baseURL}/investor_relations.html"><i class="fa fa-users" aria-hidden="true"></i>Investors</a></li>
				<li><a id="btn-engine" data-toggle="modal" data-target="#btn_engine"><i class="btn-suscr glyphicon glyphicon-plus"></i><span>Add to Search</span></a></li>
				<li><a id="btn-homepage" data-toggle="modal" data-target="#btn_homepage"><i class="btn-suscr glyphicon glyphicon-home"></i>Set as Home</a></li>
			</ul>
			<div class="social-ft">
				<a class="share-square ss-fb" target="_blank" href="https://www.facebook.com/izsearchcom/" title="Follow us on FaceBook"></a>
				<a class="share-square ss-tw" target="_blank" href="https://twitter.com/izsearch" title="Follow us on Twitter"></a>
				<a class="share-square ss-ytb" target="_blank" href="https://www.youtube.com/channel/UCNDCQTaWEUx1P9466WLnv1A" title="Follow us on Youtube"></a>
			</div>
			<p class="cc">© 2017 iZSearch</p>
		</div>
	</div>
</div>

<div id="search_cont">
<a class="logo" href="/"><img src="${baseURL}/resources/img/logo_search.svg" alt="Logo: iZSearch.com"></a>

<form id="search-form" role="search" action="${baseURL}/search" method="GET" target="_self" accept-charset="UTF-8">
	<input id="input-search" class="form-control" type="text" name="q"  value="<c:out value="${requestScope.query}" default=""/>" autofocus autocomplete="off">
	<span id="clear" style="display: none;">×</span>
	<button class="btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
	<div id="autosuggest" class="form-group">
		<ul id="autocomplete" class="autocomplete"></ul>
	</div>
	<span class="cat-select"><a class="show-hide-sub-menu" href="javascript://"><img src="${baseURL}/resources/img/nav/menu.png" class=".icon-inert"></a></span>
</form>
	<ul class="sub-menu hide">
		<li class="the-art"><a href="${art_url}"><img src="/resources/img/nav/art.svg"><span>Art</span></a></li>
		<li class="the-books"><a href="${books_url}"><img src="/resources/img/nav/books.svg"><span>Books</span></a></li>
		<li class="the-blogs_magazines"><a href="${blogs_magazines_url}"><img src="/resources/img/nav/blogs_magazines.svg"><span>Blogs &amp; Magazines</span></a></li>
		<li class="the-business"><a href="${business_url}"><img src="/resources/img/nav/business.svg"><span>Business</span></a></li>
		<li class="the-cars"><a href="${cars_url}"><img src="/resources/img/nav/cars.svg"><span>Cars</span></a></li>
		<li class="the-celebrity"><a href="${celebrity_url}"><img src="/resources/img/nav/celebrity.svg"><span>Celebrity</span></a></li>
		<li class="the-craft"><a href="${craft_url}"><img src="/resources/img/nav/craft.svg"><span>Craft</span></a></li>
		<li class="the-design"><a href="${design_url}"><img src="/resources/img/nav/design.svg"><span>Design</span></a></li>
		<li class="the-education"><a href="${education_url}"><img src="/resources/img/nav/education.svg"><span>Education</span></a></li>
		<li class="the-family"><a href="${family_url}"><img src="/resources/img/nav/family.svg"><span>Family</span></a></li>
		<li class="the-food_recipes"><a href="${food_recipes_url}"><img src="/resources/img/nav/food_recipes.svg"><span>Food &amp; Recipes</span></a></li>
		<li class="the-fun"><a href="${fun_url}"><img src="/resources/img/nav/fun.svg"><span>Fun</span></a></li>
		<li class="the-gifts"><a href="${gifts_url}"><img src="/resources/img/nav/gifts.svg"><span>Gifts</span></a></li>
		<li class="the-health"><a href="${health_url}"><img src="/resources/img/nav/health.svg"><span>Health</span></a></li>
		<li class="the-home"><a href="${home_url}"><img src="/resources/img/nav/home.svg"><span>Home &amp; Garden</span></a></li>
		<li class="the-jobs"><a href="${jobs_url}"><img src="/resources/img/nav/jobs.svg"><span>Jobs</span></a></li>
		<li class="the-industrial"><a href="${industrial_url}"><img src="/resources/img/nav/industrial.svg"><span>Industrial</span></a></li>
		<li class="the-kids"><a href="${kids_url}"><img src="/resources/img/nav/kids.svg"><span>Kids</span></a></li>
		<li class="the-law"><a href="${law_url}"><img src="/resources/img/nav/law.svg"><span>Law</span></a></li>
		<li class="the-lifestyle"><a href="${lifestyle_url}"><img src="/resources/img/nav/lifestyle.svg"><span>Lifestyle</span></a></li>
		<li class="the-men"><a href="${men_url}"><img src="/resources/img/nav/men.svg"><span>Men</span></a></li>
		<li class="the-movies"><a href="${movies_url}"><img src="/resources/img/nav/movies.svg"><span>Movies</span></a></li>
		<li class="the-music"><a href="${music_url}"><img src="/resources/img/nav/music.svg"><span>Music</span></a></li>
		<li class="the-news"><a href="${news_url}"><img src="/resources/img/nav/news.svg"><span>News</span></a></li>
		<li class="the-npo"><a href="${npo_url}"><img src="/resources/img/nav/npo.svg"><span>NPO</span></a></li>
		<li class="the-pets"><a href="${pets_url}"><img src="/resources/img/nav/pets.svg"><span>Pets</span></a></li>
		<li class="the-politics"><a href="${politics_url}"><img src="/resources/img/nav/politics.svg"><span>Politics</span></a></li>
		<li class="the-real_estate"><a href="${real_estate_url}"><img src="/resources/img/nav/real_estate.svg"><span>Real Estate</span></a></li>
		<li class="the-science"><a href="${science_url}"><img src="/resources/img/nav/science.svg"><span>Science</span></a></li>
		<li class="the-seniors"><a href="${seniors_url}"><img src="/resources/img/nav/seniors.svg"><span>Seniors</span></a></li>
		<li class="the-shopping"><a href="${shopping_url}"><img src="/resources/img/nav/shopping.svg"><span>Shopping</span></a></li>
		<li class="the-sports"><a href="${sports_url}"><img src="/resources/img/nav/sports.svg"><span>Sports</span></a></li>
		<li class="the-style_fashion"><a href="${style_fashion_url}"><img src="/resources/img/nav/style_fashion.svg"><span>Style &amp; Fashion</span></a></li>
		<li class="the-teachers"><a href="${teachers_url}"><img src="/resources/img/nav/teachers.svg"><span>Teachers</span></a></li>
		<li class="the-tech"><a href="${tech_url}"><img src="/resources/img/nav/tech.svg"><span>Technology</span></a></li>
		<li class="the-teens"><a href="${teens_url}"><img src="/resources/img/nav/teens.svg"><span>Teens</span></a></li>
		<li class="the-tips_tutorials"><a href="${tips_tutorials_url}"><img src="/resources/img/nav/tips_tutorials.svg"><span>Tips &amp; Tutorials</span></a></li>
		<li class="the-travel"><a href="${travel_url}"><img src="/resources/img/nav/travel.svg"><span>Travel</span></a></li>
		<li class="the-tools"><a href="${tools_url}"><img src="/resources/img/nav/tools.svg"><span>Tools</span></a></li>
		<li class="the-tv"><a href="${tv_url}"><img src="/resources/img/nav/tv.svg"><span>TV</span></a></li>
		<li class="the-women"><a href="${women_url}"><img src="/resources/img/nav/women.svg"><span>Women</span></a></li>
	</ul>
	<div id="text_cont">Promote your business with <a href="${baseURL}/advertise.html"><u>iZAds</u></a></div>


<div id="tab-container">
	<!-- Nav tabs -->
	<div id="bloc_menu">
		<div id="cts-menu">
			<ul class="nav active-menu" role="tablist">
				<li role="presentation" data-name="news"class="active"><a href="#" aria-controls="home" role="tab" data-toggle="tab">News</a></li>
				<li role="presentation" data-name="blogs_magazines"><a href="#" aria-controls="profile" role="tab"  data-toggle="tab">Blogs</a></li>
				<li role="presentation" data-name="tech"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Tech</a></li>
				<li role="presentation" data-name="business"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Business</a></li>
				<li role="presentation" data-name="sports"><a href="#" aria-controls="profile" role="tab"  data-toggle="tab">Sports</a></li>
				<li role="presentation" data-name="cars"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Cars</a></li>
				<li role="presentation" data-name="travel"><a href="" aria-controls="profile" role="tab" data-toggle="tab">Travel</a></li>
				<li role="presentation" data-name="food_recipes"><a href="" aria-controls="profile" role="tab" data-toggle="tab">Food</a></li>
				<li role="presentation" data-name="health"><a href="" aria-controls="profile" role="tab" data-toggle="tab">Health</a></li>
				<li role="presentation" data-name="shopping"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Shopping</a></li>
				<li role="presentation" data-name="science"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Science</a></li>
				<li role="presentation" data-name="politics"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Politics</a></li>
				<li role="presentation" data-name="celebrity"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Celebrity</a></li>
				<li role="presentation" data-name="movies"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Movies</a></li>
				<li role="presentation" data-name="music"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Music</a></li>
				<li role="presentation" data-name="lifestyle"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Lifestyle</a></li>
				<li role="presentation" data-name="style_fashion"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Fashion</a></li>
				<li role="presentation" data-name="fun"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Fun</a></li>
				<li role="presentation" data-name="pets"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Pets</a></li>
				<li role="presentation" data-name="men"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Men</a></li>
				<li role="presentation" data-name="women"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Women</a></li>
				<li role="presentation" data-name="teachers"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Teachers</a></li>
				<li role="presentation" data-name="education"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Education</a></li>
				<li role="presentation" data-name="books"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Books</a></li>
				<li role="presentation" data-name="craft"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Craft</a></li>
				<li role="presentation" data-name="family"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Family</a></li>
				<li role="presentation" data-name="gifts"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Gifts</a></li>
				<li role="presentation" data-name="home"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Home</a></li>
				<li role="presentation" data-name="jobs"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Jobs</a></li>
				<li role="presentation" data-name="kids"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Kids</a></li>
				<li role="presentation" data-name="law"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">Law</a></li>
				<li role="presentation" data-name="npo"><a href="#" aria-controls="profile" role="tab" data-toggle="tab">NPO</a></li>
			</ul>
		</div>
	</div>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active"  id="preview" ></div></div>
</div>
	<a id="back-to-top" href="#" class="btn btn-lg back-to-top" role="button" ><!--title="Click to return on the top page" data-toggle="tooltip" data-placement="left"--><span class="glyphicon glyphicon-chevron-up"></span></a>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#show_info').click(function(event) {
			$('.sidebar').toggleClass('active');


		});
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function($) {

		$('#show_info_close').click(function(event) {
			$('.sidebar').removeClass('active');
			$('#header a.logo').toggleClass('center');
		});
		$('a.show-more').click(function () {
			$('#header > .submenu > li.pm').each(function (index, domElement) {
				$(domElement).toggleClass('hide');
			});
		});

		// Bug Report Form
		$('ul.submenu > li.pm > a[data-object="#rsads-wrapper"]').click(function () {
			$('#rsads-wrapper').toggleClass('hide');
		});

		// Active menu: show more
		$('a.show-hide-sub-menu').click(function () {
			var self = $(this);
			if (self.hasClass('glyphicon-chevron-down')) {
				self.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
				$('#search_cont').css('padding-top', '0');

			} else if (self.hasClass('glyphicon-chevron-up')) {
				self.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');

			}
			$('#search_cont').toggleClass('sub_menu_open');

			$('ul.sub-menu').toggleClass('hide');

		});
		$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});

		// Scroll body to 0px on click
		$('#back-to-top').click(function () {
			$('#back-to-top').tooltip('hide');
			$('body,html').animate({ scrollTop: 0 }, 800);
			return false;
		});

		$('#back-to-top').tooltip('show');

		//Кнопка удаления содержимого строки запроса
		$('#input-search').keyup(function() {
			var cont = $('#input-search').val();
			if(cont.length != 0 ) {
				$('#clear').removeAttr('style');
			}else{
				$('#clear').css('display','none');
			}
		});
		$("#clear").click(function(){
			$('#input-search').val('');
			$('#clear').css('display','none');
		})
	});
</script>
</div>
