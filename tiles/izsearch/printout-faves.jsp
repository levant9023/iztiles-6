
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/browses.css" />


<c:set var="faves_map" value="${requestScope.faves}" scope="page"/>
<%--${faves_map['debstr']}--%>
<div class="blogblock wpanel">
  <c:set var="count" value="1" />
  <c:set var="faves" value="${faves_map['faves']}" />
  <c:set var="others" value="${faves_map['others']}" />

  <%-- View for Faves with high rank --%>
  <c:if test="${not empty faves}" >
    <c:forEach var="fave" items="${faves}" varStatus="faves_status">
      <c:set var="cap" value="${fn:toUpperCase(fn:substring(fave.key, 0, 1))}" />
      <c:set var="body" value="${fn:substring(fave.key, 1, fn:length(fave.key))}" />
      <ul class="faves-list">
        <h3 class="underlinedh3"><c:out value="${cap}${body}" default="N/A"/></h3>
        <c:forEach var="scored_item" items="${fave.value}">
          <c:set var="item" value="${scored_item.faves}" />
          <c:set var="title" value="${item.title}" />
          <li id="${item.id}" data-dc="${count}" data-di="${faves_status.index}"
              title="${item.description}" class="${status}">
            <c:set var="img_url" value="/images/faves/${item.id}_${item.title}.jpg" />
            <c:if test="${not empty item.directUrl}" >
              <a href="${item.directUrl}" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/${item.id}']);">
            </c:if>
            <c:if test="${empty item.directUrl}" >
              <a href="${item.url}" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/${item.id}']);">
            </c:if>
              <img src="${img_url}" alt="image" />
            </a>
            <%--${scored_item.source} ${scored_item.score}--%>
          </li>
          <c:set var="count" value="${count+1}" />
        </c:forEach>
      </ul>
    </c:forEach>
  </c:if>

  <%-- View for Faves with no rank --%>
  <c:if test="${not empty others}" >
    <ul class="faves-list other">
      <h3 class="underlinedh3"><c:out value="Other" /></h3>
      <c:set var="count" value="1" />
      <c:forEach var="scored_item" items="${others}" varStatus="faves_status">
        <c:set var="item" value="${scored_item.faves}" />
        <c:set var="title" value="${item.title}" />
        <li id="${item.id}" data-dc="${count}" data-di="${faves_status.index}"
            title="${item.description}">
          <c:set var="img_url" value="/images/faves/${item.id}_${item.title}.jpg" />
          <c:if test="${not empty item.directUrl}" >
            <a href="${item.directUrl}" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/${item.id}']);">
          </c:if>
          <c:if test="${empty item.directUrl}" >
            <a href="${item.url}" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/${item.id}']);">
          </c:if>
            <img src="${img_url}" alt="image">
          </a>
          <%--${scored_item.source} ${scored_item.score}--%>
        </li>
        <c:set var="count" value="${count+1}" />
      </c:forEach>
    </ul>
  </c:if>
</div>
