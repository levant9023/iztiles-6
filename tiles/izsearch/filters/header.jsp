<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<%--
  Document: header
  User:     eFanchik
  Date:     07.07.14
--%>

<%
  Date now = new Date();
  Calendar cal = Calendar.getInstance();
  cal.setTime(now);

  cal.add(Calendar.DAY_OF_MONTH, -1);
  Date past_day = cal.getTime();

  cal.add(Calendar.DAY_OF_MONTH, -7);
  Date past_week = cal.getTime();

  cal.add(Calendar.MONTH, -1);
  Date past_month = cal.getTime();

  cal.add(Calendar.YEAR, -1);
  Date past_year = cal.getTime();

  SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

  pageContext.setAttribute("now", sdf.format(now));
  pageContext.setAttribute("past_day", sdf.format(past_day));
  pageContext.setAttribute("past_week", sdf.format(past_week));
  pageContext.setAttribute("past_month", sdf.format(past_month));
  pageContext.setAttribute("past_year", sdf.format(past_year));
%>

<c:set var="count" value="${(empty requestScope.c) ? 10 : requestScope.c}" scope="request"/>
<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="langs" value="${empty requestScope.q ? '' : requestScope.langs}" scope="request"/>
<c:set var="type" value="${empty requestScope.t ? '' : requestScope.t}" scope="request"/>
<c:set var="date_start" value="${empty requestScope.start ? '' : requestScope.start}" scope="request"/>
<c:set var="date_stop" value="${empty requestScope.stop ? '' : requestScope.stop}" scope="request"/>
<c:set var="approximativeResults" value="${requestScope.approximativeResults}" scope="request"/>
<c:set var="maininfo" value="${requestScope.infotest}" scope="request"/>
<c:set var="fullPath" value="${requestScope['javax.servlet.forward.request_uri']}"/>
<c:set var="requestURL" value="${baseURL}${requestScope.wpath}" scope="request"/>

<c:if test="${requestScope.wpath == '/bugreport.html'}" var="tst">
  <c:set var="requestURL" value="${baseURL}"/>
</c:if>

<c:set var="show-filter" value="false"/>
<c:if test="${requestScope.wpath == '/news.html'}">
  <c:set var="show-filter" value="true"/>
</c:if>

<c:set var="language" value="${cookie['_language'].value}"/>
<c:set var="country" value="${cookie['_country'].value}"/>
<c:set var="sort" value="${cookie['_sort'].value}"/>
<c:set var="start" value="${cookie['_start_date'].value}"/>
<c:set var="end" value="${cookie['_end_date'].value}"/>

<c:set var="sortTitle" value="sort by date"/>
<c:set var="langTitle" value="by language/country"/>
<c:choose>
  <c:when test="${empty sort}">
    <c:set var="sort" value="date"/>
    <c:set var="sortTitle" value="sort by date"/>
  </c:when>
  <c:otherwise>
    <c:if test="${sort eq 'date'}">
      <c:set var="sort" value=""/>
      <c:set var="sortTitle" value="sort by relevance"/>
    </c:if>
  </c:otherwise>
</c:choose>

<c:choose>
  <c:when test="${empty language and empty country}">
    <c:set var="langTitle" value="by language/country"/>
  </c:when>
  <c:when test="${language eq 'en' and empty country}">
    <c:set var="langTitle" value="by language/country"/>
  </c:when>
  <c:when test="${not empty language and empty country}">
    <c:set var="langTitle" value="${language} | all"/>
  </c:when>
  <c:otherwise>
    <c:set var="langTitle" value="${language} | ${langs[country]}"/>
  </c:otherwise>
</c:choose>

<c:url var="sortUrl" value="${baseURL}">
  <c:param name="q" value="${query}"/>
  <c:param name="start" value="${date_start}"/>
  <c:param name="stop" value="${date_stop}"/>
  <c:param name="sort" value="${sort}"/>
</c:url>

<div id="header" class="row" ${header_style}>

  <nav class="navbar navbar-default no-back" role="navigation">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <c:set var="logo" value="${iz:basename(requestScope['javax.servlet.forward.request_uri'])}"/>
      <div id="logo" class="navbar-header">
        <a href="${baseURL}" class="navbar-brand">
          <img class="logo" src="${baseURL}/resources/img/${logo}.png" alt="logo">
        </a>
      </div>
      <div id="search-panel" class="form-group">
        <form id="search-form" class="navbar-form navbar-left" role="search"
              action="${requestURL}" method="GET" target="_self">
          <input id="input-search" type="text" class="form-control" autocomplete="off" name="q"
                 value="<c:out value="${query}"/>" autofocus>
          <button id="btn-search" type="submit" class="input-inside">
            <img src="${baseURL}/resources/img/lence_search.png" alt="lence">
          </button>
          <div id="autosuggest" class="form-group">
            <ul id="autocomplete" class="autocomplete"></ul>
          </div>
        </form>

        <c:if test="${requestScope.wpath eq '/news.html'}" var="ns">
          <!-- Any time/date/lang sort filter -->
          <ul id="filter-panel" class="nav nav-pills">
          <li id="anytime" class="dropdown">
            <a id="btnExpand" data-behavior="menu" data-toggle="dropdown"
               title="Expand all sections">Any time<span class="caret"></span>
            </a>
            <ul class="mainmenu">
              <li>
                <%--<i class="fa fa-check" aria-hidden="true"></i>--%>
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                <a id="anymenuitem" href="${baseURL}?q=${query}">Any Time</a>
              </li>
              <li>
                <span class="glyphicon" aria-hidden="true"></span>
                <a id="daymenuitem" href="${baseURL}?q=${query}&start=${past_day}&stop=${now}">Past Day</a>
              </li>
              <li>
                <span class="glyphicon" aria-hidden="true"></span>
                <a id="weekmenuitem" href="${baseURL}?q=${query}&start=${past_week}&stop=${now}">Past Week</a>
              </li>
              <li>
                <span class="glyphicon" aria-hidden="true"></span>
                <a id="monthmenuitem" href="${baseURL}?q=${query}&start=${past_month}&stop=${now}">Past Month</a>
              </li>
              <li>
                <span class="glyphicon" aria-hidden="true"></span>
                <a id="yearmenuitem" href="${baseURL}?q=${query}&start=${past_year}&stop=${now}">Past Year</a>
              </li>
              <li>
                <span class="glyphicon" aria-hidden="true"></span>
                <a  id="specify-menu" class="right-caret">Specify</a>
                <ul id="specify-submenu" class="submenu">
                  <li class="form">
                    <form id="period-form" role="form" action="${baseURL}/?q=${query}" method="POST" target="_self">
                      <div class="form-group">
                        <label for="period-from">Date from:</label>
                        <div id="datepicker1" class='input-group input-group-sm date datepicker'>
                          <input type='text' data-date-format="YYYY-MM-DD"
                                 class="form-control"
                                 id="period-from"
                                 name="start" value="" title="Date in format of YYYY-MM-DD (1972-05-21)" required pattern="^(\d{4})-(\d{2})-(\d{2})$"/>
                          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <span class="info"></span>
                      </div>
                      <div class="form-group">
                        <label for="period-to">Date to:</label>
                        <div id="datepicker2" class='input-group input-group-sm date datepicker'>
                          <input type='text' data-date-format="YYYY-MM-DD"
                                 class="form-control"
                                 id="period-to" name="end" value=""
                                 title="Date in format of YYYY/MM/DD (1972-05-21)" required pattern="^(\d{4})-(\d{2})-(\d{2})$" />
                          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <span class="info"></span>
                      </div>
                      <%--<div class="form-group">
                        <input id="btn-anytime-submit" type="button" name="anytime-submit" class="btn btn-info btn-xs"
                               value="Send">
                      </div>--%>
                    </form>
                  </li>
                </ul>
              </li>
            </ul>
          </li>

          <li id="lang-sort">
            <div id="lang-popover-wrapper">
              <a id="btn-sort-by-lang" class="btnIconLeft" data-toggle="popover" role="popover">
                <span><c:out value="${langTitle}"/></span>
              </a>
              <div id="lang-popover" class="hide">
                <div class="arrow"></div>
                <h3 class="popover-title"></h3>
                <div class="popover-content">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <span class="simg glyphicon glyphicon-globe">&nbsp;</span>
                      <h3 class="feedback-title">Sort by Language/Country</h3>
                    </div>
                    <div class="panel-body">
                      <form class="izwidget" role="form">
                        <div class="form-group">
                          <ul class="language-list">
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="language" id="rdl_en" value="en" checked>english</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="language" id="rdl_us" value="us">english/us</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="language" id="rdl_de" value="de">deutsch</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="language" id="rdl_es" value="es">español</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="language" id="rdl_fr" value="fr">français</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="language" id="rdl_it" value="it">italiano</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="language" id="rdl_ru" value="ru">русский</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="language" id="rdl_cn" value="cn">&#20013;&#25991;&#32;&#40;&#31616;&#20307;&#41;</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="language" id="rdl_ja" value="ja">&#26085;&#26412;&#35486;</label>
                              </div>
                            </li>
                          </ul>
                        </div>

                        <div class="form-group">
                          <ul class="country-list">
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_all" value="" checked>All</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_us" value="us">United State</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_uk" value="uk">United Kingdom</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_au" value="au">Australia</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_br" value="br">Brasilia</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_ca" value="ca">Canada</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_cn" value="cn">China</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_de" value="de">Germany</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_es" value="es">Spain</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_fr" value="fr">France</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_it" value="it">Italy</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_ja" value="ja">Japan</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_kr" value="kr">Korea</label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label><input type="radio" name="country" id="rdc_ru" value="ru">Russia</label>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <button type="button" class="lc-form-reset">Reset</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li id="date-sort">
            <a id="btnSortByDate" class="btnIconLeft"><span><c:out value="${sortTitle}"/></span></a>
          </li>
        </ul>
        </c:if>
      </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="nvb-01">

          <ul id="search-toolbar" class="nav navbar-nav navbar-right">
            <li class="first">

              <!-- Advanced button -->
              <div id="btg-advanced" class="btn-group">
                <a href="javascript://" class="btn btn-default btn-sm" title="Advanced search"
                   data-title="Advanced search filter"
                   data-toggle="search-nav" role="popover">
                  <span class="glyphicon glyphicon-flag"></span>
                </a>

                <div class="popover-content hide">
                  <div id="advanced-row">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <span class="simg glyphicon glyphicon-search">&nbsp;</span>

                        <h3 class="feedback-title advSearch">Advanced search</h3>
                      </div>
                      <div class="panel-body">
                        <form id="advanced-form"
                              class="form-horizontal"
                              role="form" action="${baseURL}/advs.html"
                              method="POST">
                          <div class="form-group form-group-xs">
                            <label for="inputAllWords" class="col-sm-4 control-label">with all words:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="inputAllWords" name="allWords">
                            </div>
                          </div>
                          <div class="form-group form-group-xs">
                            <label for="inputExactPhrase" class="col-sm-4 control-label">with exact phrase:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="inputExactPhrase" name="exactPhrase">
                            </div>
                          </div>
                          <div class="form-group form-group-xs">
                            <label for="inputAnyWords" class="col-sm-4 control-label">with any of words:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="inputAnyWords" name="anyWords">
                            </div>
                          </div>
                          <div class="form-group form-group-xs">
                            <label for="inputWithoutWords" class="col-sm-4 control-label">without words:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="inputWithoutWords" name="withoutWords">
                            </div>
                          </div>
                          <div class="form-group form-group-xs">
                            <label for="inputOnSite" class="col-sm-4 control-label">on site:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="inputOnSite" name="onSite">
                            </div>
                          </div>
                          <div id="cbx-sw" class="form-group form-group-xs">
                            <label for="inputSW" class="col-sm-7 control-label">Search within: </label>
                            <div class="col-sm-2">
                              <input type="checkbox" id="inputSW" name="searchWith" checked>
                            </div>
                          </div>
                          <div id="cbx-cs" class="form-group form-group-xs">
                            <label for="inputCS" class="col-sm-4 control-label">Case sensitive: </label>
                            <div class="col-sm-2">
                              <input type="checkbox" id="inputCS" name="caseSensitive" checked>
                            </div>
                          </div>
                          <div id="advexample">(Example: <a href="javascript://"> drugs "breast cancer" (gene OR DNA) site:
                            wikipedia.org</a>)
                          </div>
                          <hr>
                          <input id="isAdvanced" name="isAdvanced" value="true" type="hidden">
                          <button id="btn_advanced" type="submit" class="btn btn-primary btn-sm" name="btn-advanced">Send<span
                              class="glyphicon glyphicon-arrow-right right"></span>
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Tools button
              <div id="btg-tols" class="btn-group">
                <a class="btn btn-default btn-sm dropdown-toggle disabled"
                   data-toggle="search-nav" role="popover"
                   aria-haspopup="true"
                   title="Tools">
                  <span class="glyphicon glyphicon-wrench"></span>
                </a>

                <div class="popover-content hide">
                  <div id="tols-row">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <span class="simg glyphicon glyphicon-wrench">&nbsp;</span>

                        <h3 class="feedback-title">Tools</h3>
                      </div>
                      <div class="panel-body">
                        <ul>
                          <li>
                            <div class="sptitle"><a>Region Filter</a></div>
                            <div class="comment"></div>
                          </li>
                          <li>
                            <span class="sptitle"><a>Domains Filter</a></span>
                            <span class="comment"></span>
                          </li>
                          <li>
                            <div class="sptitle"><a>Flexible Search</a></div>
                            <div class="spcomment">(with matrix of similar or related words)</div>
                          </li>
                          <li>
                            <span class="sptitle"><a>Data Search</a></span>
                            <span class="comment"></span>
                          </li>
                          <li>
                            <span class="sptitle"><a>Browse</a></span>
                            <span class="comment"></span>
                          </li>
                          <li>
                            <span class="sptitle"><a>Surprise Me!</a></span>
                            <span class="comment"></span>
                          </li>
                          <li>
                            <span class="sptitle"><a>I Want More!</a></span>
                            <span class="comment"></span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              -->

              <!-- Web
              <div id="btg-web" class="btn-group">
                <a class="btn btn-default btn-sm disabled"
                   data-toggle="search-nav" role="popover"
                   title="Web">
                  <span class="glyphicon glyphicon-globe"></span>
                </a>

                <div class="popover-content hide">
                  <div id="web-row">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <span class="simg glyphicon glyphicon-globe">&nbsp;</span>

                        <h3 class="feedback-title">Languages</h3>
                      </div>
                      <div class="panel-body">
                        <form class="izwidget" role="form">
                          <ul class="region-list">
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_de" id="rdDE" value="de">
                                  Deutsch
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_en" id="rdEN" value="en" checked>
                                  English
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_es" id="rdES" value="es">
                                  español
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_gr" id="rdGR" value="gr">
                                  Ελληνικά
                                </label>
                              </div>
                            </li>
                            <li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_fr" id="rdFR" value="fr">
                                  français
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_hr" id="rdHR" value="hr">
                                  hrvatski
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_it" id="rdIT" value="it">
                                  italiano
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_nl" id="rdNL" value="nl">
                                  Nederlands
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_pl" id="rdPL" value="pl">
                                  polski
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_pt" id="rdPT" value="pt">
                                  português
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_ro" id="rdRO" value="ro">
                                  română
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_ru" id="rdRU" value="ru">
                                  русский
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_vi" id="rdVI" value="vi">
                                  Ti&#7871;ng Vi&#7879;t
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_tr" id="rdTR" value="tr">
                                  Türkçe
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_ua" id="rdUA" value="ua">
                                  українська
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_ar" id="rdAR" value="ar">
                                  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_th" id="rdTH" value="th">
                                  &#3652;&#3607;&#3618;
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_ko" id="rdKO" value="ko">
                                  &#54620;&#44397;&#50612;
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_cn" id="rdCN" value="cn">
                                  &#20013;&#25991;&#32;&#40;&#31616;&#20307;&#41;
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_tw" id="rdTW" value="tw">
                                  &#20013;&#25991;&#32;&#40;&#32321;&#39636;&#41;
                                </label>
                              </div>
                            </li>
                            <li>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="rd_ja" id="rdJA" value="ja">
                                  &#26085;&#26412;&#35486;
                                </label>
                              </div>
                            </li>
                          </ul>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              -->

              <!-- Compare button -->
              <div id="btg-compare" class="btn-group">
                <a id="btn-google" href="javascript://" class="btn btn-default btn-sm"
                   data-toggle="search-nav" role="popover"
                   title="Compare (with Google search)">
                  <span class="glyphicon glyphicon-eye-open"></span>
                </a>

                <div class="popover-content hide">
                  <div id="compare-row">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <span class="simg glyphicon glyphicon-send"></span>

                        <h3 class="feedback-title">Compare results with Google</h3>
                      </div>
                      <div class="panel-body">
                        <ul class="list-menu"></ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

                <%--
                <!-- Feedback button -->
                <div id="btg-feedback" class="btn-group">
                  <a href="javascript:" class="btn btn-default btn-sm disabled"
                     data-toggle="search-nav" role="popover"
                     title="Feedback">
                    <span class="glyphicon glyphicon-envelope"></span>
                  </a>

                  <div class="popover-content hide">
                    <div id="feedback-row">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <span class="simg glyphicon glyphicon-send"></span>

                          <h3 class="feedback-title">Report bad results</h3>
                        </div>
                        <div class="panel-body">
                          <form class="izwidget" role="form">
                            <div class="form-group">
                              <label for="inputExactlySearch" class="control-label">This is exactly what I searched
                                for:</label>
                              <input type="text" class="form-control" id="inputExactlySearch" name="exactlySearch">
                            </div>
                            <div class="form-group">
                              <label for="inputPagesToFind" class="control-label">The page(s) I wanted to find were:</label>
                              <textarea class="form-control" rows="2" id="inputPagesToFind" name="pagesToFind"></textarea>
                            </div>
                            <div class="form-group">
                              <label for="inputBadReason" class="control-label">The reason(s) the results seemed bad
                                are:</label>
                              <textarea class="form-control" rows="2" id="inputBadReason" name="badReason"></textarea>
                            </div>
                            <div class="form-group">
                              <label for="inputFeedbackEmail" class="control-label">Your email (not required):</label>
                              <input type="email" class="form-control" id="inputFeedbackEmail" name="email">
                            </div>
                            <div class="form-group">
                              <label for="inputOpinion" class="control-label">Where did you hear about izSearch?</label>
                              <input type="text" class="form-control" id="inputOpinion" name="opinion">
                            </div>
                            <div class="form-group">
                              <label for="feedback-valid-field" class="control-label"></label>
                              <input id="feedback-valid-field" type="text" name="validator" value="valid">
                            </div>
                            <div class="form-group">
                              <div class="btn-send">
                                <button type="submit" class="btn btn-primary btn-block">
                                  <span class="glyphicon glyphicon-envelope left"></span>Send
                                  <span class="glyphicon glyphicon-arrow-right right"></span>
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                --%>

              <!-- Redmine button -->
              <div id="btg-bugfix" class="btn-group">
                <a id="remine-link" href="javascript:" class="btn btn-default btn-sm"
                   data-toggle="search-nav" role="popover" data-history="false"
                   title="Bug report">
                  <span class="glyphicon glyphicon-envelope"></span>
                </a>

                <div class="popover-content hide">
                  <div id="redmine-report">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <span class="simg glyphicon glyphicon-send"></span>
                        <h3 class="bugfix-title">Report bugs</h3>
                      </div>
                      <div class="panel-body">
                        <form id="form-redmine" class="izwidget" role="form">
                          <div class="form-group">
                            <label for="inputUserName" class="control-label">Your name (required)*:</label>
                            <input type="text" class="form-control" id="inputUserName" name="username" value="">
                            <span id="username_message" class="message"></span>
                          </div>
                          <div class="form-group">
                            <label for="inputBugEmail" class="control-label">Your email (required)*:</label>
                            <input type="email" class="form-control" id="inputBugEmail" name="usermail">
                            <span id="usermail_message" class="message"></span>
                          </div>
                          <div class="form-group">
                            <label for="inputBugSubject" class="control-label">Subject (required) *:</label>
                            <input type="text" class="form-control" id="inputBugSubject" name="bugsubject"/>
                            <span id="bugsubject_message" class="message"></span>
                          </div>
                          <div class="form-group">
                            <label for="inputBugDescr" class="control-label">Bug description (required)*:</label>
                            <textarea class="form-control" rows="2" id="inputBugDescr" name="bugdescr"></textarea>
                            <span id="bugdescr_message" class="message"></span>
                          </div>
                          <div class="form-group">
                            <div class="btn-send">
                              <span id="redmine-result-message" class="message"></span>
                              <button id="btn-redmine" type="button" class="btn btn-primary btn-block">
                                <span class="glyphicon glyphicon-envelope left"></span>Send
                                <span class="glyphicon glyphicon-arrow-right right"></span>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


              <!-- Redmine report button -->
              <c:if test="${empty requestScope.page}" var="page_">
                <c:set value="1" var="page"/>
              </c:if>
              <c:url var="redmine_report_url" value="bugreport.html">
                <c:param name="page" value="${page}"/>
              </c:url>
              <div id="redmine-report-link" class="btn-group">
                <a id="redmine-blog" href="${redmine_report_url}" class="btn btn-default btn-sm"
                   title="Bug report and Discussion Forum">
                  <span class="glyphicon glyphicon-list-alt"></span>
                </a>
              </div>

                <%--
                <!-- Settings button -->
                <div id="btg-getInstance" class="btn-group btn-group-xs">
                  <a href="javascript://" class="btn btn-default btn-sm disabled"
                     data-toggle="search-nav" role="popover"
                     title="Settings">
                    <span class="glyphicon glyphicon-cog"></span>
                  </a>

                  <div class="popover-content hide">
                    <div id="getInstance-row">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <span class="simg glyphicon glyphicon-cog"></span>

                          <h3 class="feedback-title">Settings</h3>
                        </div>
                        <div class="panel-body">
                          <form id="frm-getInstance" class="form-horizontal izwidget" role="form">
                            <div class="form-group">
                              <label for="inputRegion" class="col-sm-6 control-label">
                                <div class="title">Regions:</div>
                                <div class="body">Show region specific resuts</div>
                              </label>

                              <div class="col-sm-6">
                                <select class="form-control input-sm" id="inputRegion" name="regions">
                                  <option value="wt-wt">No Region Selected</option>
                                  <option value="ar-es">Argentina</option>
                                  <option value="at-de">Austria</option>
                                  <option value="au-en">Australia</option>
                                  <option value="be-fr">Belgium (fr)</option>
                                  <option value="be-nl">Belgium (nl)</option>
                                  <option value="bg-bg">Bulgaria</option>
                                  <option value="br-pt">Brazil</option>
                                  <option value="ca-en">Canada</option>
                                  <option value="ca-fr">Canada (fr)</option>
                                  <option value="ch-de">Switzerland (de)</option>
                                  <option value="ch-fr">Switzerland (fr)</option>
                                  <option value="ch-it">Switzerland (it)</option>
                                  <option value="cl-es">Chile</option>
                                  <option value="cn-zh">China</option>
                                  <option value="co-es">Colombia</option>
                                  <option value="ct-ca">Catalonia</option>
                                  <option value="cz-cs">Czech Republic</option>
                                  <option value="de-de">Germany</option>
                                  <option value="dk-da">Denmark</option>
                                  <option value="ee-et">Estonia</option>
                                  <option value="es-ca">Spain (ca)</option>
                                  <option value="es-es">Spain</option>
                                  <option value="fi-fi">Finland</option>
                                  <option value="fr-fr">France</option>
                                  <option value="gr-el">Greece</option>
                                  <option value="hk-tzh">Hong Kong</option>
                                  <option value="hr-hr">Croatia</option>
                                  <option value="hu-hu">Hungary</option>
                                  <option value="id-en">Indonesia (en)</option>
                                  <option value="id-id">Indonesia</option>
                                  <option value="ie-en">Ireland</option>
                                  <option value="il-he">Israel</option>
                                  <option value="in-en">India</option>
                                  <option value="it-it">Italy</option>
                                  <option value="jp-jp">Japan</option>
                                  <option value="kr-kr">Korea</option>
                                  <option value="lt-lt">Lithuania</option>
                                  <option value="lv-lv">Latvia</option>
                                  <option value="mx-es">Mexico</option>
                                  <option value="my-en">Malaysia (en)</option>
                                  <option value="my-ms">Malaysia</option>
                                  <option value="nl-nl">Netherlands</option>
                                  <option value="no-no">Norway</option>
                                  <option value="nz-en">New Zealand</option>
                                  <option value="pe-es">Peru</option>
                                  <option value="ph-en">Philippines</option>
                                  <option value="ph-tl">Philippines (tl)</option>
                                  <option value="pl-pl">Poland</option>
                                  <option value="pt-pt">Portugal</option>
                                  <option value="ro-ro">Romania</option>
                                  <option value="ru-ru">Russia</option>
                                  <option value="se-sv">Sweden</option>
                                  <option value="sg-en">Singapore</option>
                                  <option value="sk-sk">Slovakia</option>
                                  <option value="sl-sl">Slovenia</option>
                                  <option value="th-th">Thailand</option>
                                  <option value="tr-tr">Turkey</option>
                                  <option value="tw-tzh">Taiwan</option>
                                  <option value="ua-uk">Ukraine</option>
                                  <option value="uk-en">United Kingdom</option>
                                  <option value="us-en">United States</option>
                                  <option value="us-es">United States (es)</option>
                                  <option value="vn-vi">Vietnam</option>
                                  <option value="xa-ar">Saudi Arabia</option>
                                  <option value="xa-en">Saudi Arabia (en)</option>
                                  <option value="xl-es">Latin America</option>
                                  <option value="za-en">South Africa</option>
                                </select>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="inputLanguage" class="col-sm-6 control-label">
                                <div class="title">Language:</div>
                                <div class="body">Show language specific results</div>
                              </label>

                              <div class="col-sm-6">
                                <select class="form-control input-sm" id="inputLanguage" name="languages">
                                  <option value="wt_WT">Preferred language</option>
                                  <option value="ca_ES">Català a Catalunya</option>
                                  <option value="cs_CZ">Česky v České republice</option>
                                  <option value="da_DK">Dansk i Danmark</option>
                                  <option value="de_CH">Deutsch in der Schweiz</option>
                                  <option value="de_DE">Deutsch in Deutschland</option>
                                  <option value="en_AU">English in Australia</option>
                                  <option value="en_CA">English in Canada</option>
                                  <option value="en_GB">English in United Kingdom</option>
                                  <option value="en_US">English of United States</option>
                                  <option value="eo_XX">Esperanto</option>
                                  <option value="es_AR">Español de Argentina</option>
                                  <option value="es_CL">Español en Chile</option>
                                  <option value="es_CO">Español de Colombia</option>
                                  <option value="es_CR">Español de Costa Rica</option>
                                  <option value="es_EC">Español en Ecuador</option>
                                  <option value="es_ES">Español de España</option>
                                  <option value="es_MX">Español Mexicano</option>
                                  <option value="es_PE">Español Peruano</option>
                                  <option value="es_UY">Español de Uruguay</option>
                                  <option value="es_VE">Español venezolano</option>
                                  <option value="fr_BE">Français en Belgique</option>
                                  <option value="fr_CA">Français Canadien</option>
                                  <option value="fr_CH">Français de Suisse</option>
                                  <option value="fr_FR">Français en France</option>
                                  <option value="ga_IE">Gaeilge na Éireann</option>
                                  <option value="gr_GR">Ελληνικά στην Ελλάδα</option>
                                  <option value="nb_NO">Norsk bokmål i Norge</option>
                                  <option value="nl_BE">Belgisch-Nederlands</option>
                                  <option value="nl_NL">Nederlands in Nederland</option>
                                  <option value="nn_NO">Norsk nynorsk i Norge</option>
                                  <option value="pl_PL">Polski w Polsce</option>
                                  <option value="pt_BR">Português do Brasil</option>
                                  <option value="pt_PT">Português de Portugal</option>
                                  <option value="ro_RO">Romana in Romania</option>
                                  <option value="ru_RU">Русский России</option>
                                  <option value="tr_TR">Türkiye'de Türkçe</option>
                                  <option value="uk_UA">Українська</option>
                                </select>
                              </div>
                            </div>

                            <div class="hr"></div>
                            <div class="form-group">
                              <label for="inputShowPreview" class="col-sm-10 control-label">
                                <div class="title">Show Page Previews</div>
                              </label>

                              <div class="col-sm-2">
                                <div id="btg-preview" class="btn-group btn-group-xs right" data-toggle="buttons">
                                  <label class="btn btn-default active">
                                    <input type="checkbox" name="show_preview" id="inputShowPreview" checked>On
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="inputShowAlsoOn" class="col-sm-10 control-label">
                                <div class="title">Show 'Also On' Section</div>
                              </label>

                              <div class="col-sm-2">
                                <div id="btg-also" class="btn-group btn-group-xs right" data-toggle="buttons">
                                  <label class="btn btn-default active">
                                    <input type="checkbox" name="show_also_on" id="inputShowAlsoOn" checked>On
                                  </label>
                                </div>
                              </div>
                            </div>

                            <!--  <div class="hr"></div> -->
                            <div class="form-group">
                              <label class="col-sm-12 title"> Results Per Page</label>

                              <div id="slider-wrapper" class="col-sm-12">
                                <div id="slider" class="slider" style="left: 0;">
                                  <button class="btn btn-xs btn-default slider-handle"></button>
                                  <div class="slider-ruler">
                                     <span class="slider-marker" style="left:0%;">
                                       <span class="slider-digit">10</span>
                                     </span>
                                     <span class="slider-marker" style="left:10%;">
                                       <span class="slider-digit">20</span>
                                     </span>
                                     <span class="slider-marker" style="left:20%;">
                                       <span class="slider-digit">30</span>
                                     </span>
                                     <span class="slider-marker" style="left:30%;">
                                       <span class="slider-digit">40</span>
                                     </span>
                                     <span class="slider-marker" style="left:40%;">
                                       <span class="slider-digit">50</span>
                                     </span>
                                     <span class="slider-marker" style="left:100%;">
                                       <span class="slider-digit">100</span>
                                     </span>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!--    <div class="hr"></div> -->
                            <div class="form-group">
                              <label for="inputInstantSearch" class="col-sm-10 control-label">
                                <div class="title">Turn on Instant Search:</div>
                                <div class="body">Show search results as you type.</div>
                              </label>

                              <div class="col-sm-2">
                                <div id="btg-instant" class="btn-group btn-group-xs right" data-toggle="buttons">
                                  <label class="btn btn-default active">
                                    <input type="checkbox" name="instant_search" id="inputInstantSearch" checked>On
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="inputAutoSuggest" class="col-sm-10 control-label">
                                <div class="title">Auto-Suggest:</div>
                                <div class="body">Show suggestions under the search box as you type.</div>
                              </label>

                              <div class="col-sm-2">
                                <div id="btg-suggest" class="btn-group btn-group-xs right" data-toggle="buttons">
                                  <label class="btn btn-default active">
                                    <input type="checkbox" name="auto_suggest" id="inputAutoSuggest" checked>On
                                  </label>
                                </div>
                              </div>
                            </div>


                            <div class="form-group">
                              <label for="inputWhereOpen" class="col-sm-10 control-label">
                                <div class="title">New Window:</div>
                                <div class="body">Opens results in new windows/tabs</div>
                              </label>

                              <div class="col-sm-2">
                                <div id="btg-whereopen" class="btn-group btn-group-xs right" data-toggle="buttons">
                                  <label class="btn btn-default active">
                                    <input type="checkbox" name="where_open" id="inputWhereOpen" checked>On
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputShortcuts" class="col-sm-10 control-label">
                                <div class="title">Keyboard Shortcuts:</div>
                                <div class="body">Enables keyboard shortcuts on the site.</div>
                              </label>

                              <div class="col-sm-2">
                                <div id="btg-shortcut" class="btn-group btn-group-xs right" data-toggle="buttons">
                                  <label class="btn btn-default active">
                                    <input type="checkbox" name="key_shortcuts" id="inputShortcuts" checked>On
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="hr"></div>
                            <div class="form-group">
                              <label for="inputAdvert" class="col-sm-10 control-label">
                                <div class="title">Advertisements</div>
                              </label>

                              <div class="col-sm-2">
                                <div id="btg-advert" class="btn-group btn-group-xs right" data-toggle="buttons">
                                  <label class="btn btn-default active">
                                    <input type="checkbox" name="advert" id="inputAdvert" checked>On
                                  </label>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                --%>
            </li>

            <li class="last">
              <div id="btg-sh" class="btn-group">
                <a href="javascript://" class="btn btn-default btn-sm" title="Toolbar with extended option buttons"
                   data-title="Toolbar with extended option buttons"
                   data-toggle="tool-sh" role="popover">
                  <span class="glyphicon glyphicon-th"></span>
                </a>
              </div>
            </li>
          </ul>

        </div>
        <!-- /.navbar-collapse -->


    </div>
    <!-- /.container-fluid -->
  </nav>
</div>

