<%--
    Document   : spors_filter
    Created on : Nov 11, 2014, 3:26:47 AM
    Author     : efanchik
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="sectionNumber" value="0"/>
<c:forEach var="snippet" items="${snippets}" varStatus="status">
  <c:set var="item" value="${snippet}"/>
  <c:if test="${not empty item}">
    <section class="section" data-state="false" data-name="sec${sectionNumber}">
      <article class="artfirst">
        <c:set var="title" value="${iz:clear_descr(iz:text_normalize(fn:trim(item.title)))}"/>
        <c:set var="full_title" value="${title}"/>
        <c:set var="title" value="${iz:strip_str_pxl(title,' ', 'Arial', 14, 470)}"/>
        <c:set var='title' value='${iz:mark_words(title, query)}'/>
        <c:url var="url" value="${item.url}"/>
        <c:set var="domain" value="${iz:top_private_domain(url)}"/>
        <c:if test="${empty title}">
          <c:set var="title" value="${iz:clear_www(iz:get_domain(uri))}"/>
          <c:set var="title" value="${iz:cap_word(iz:text_normalize(title))}"/>
        </c:if>
        <c:set var="descr" value="${fn:trim(item.description)}"/>
        <c:if test="${empty descr}">
          <c:set var="descr" value="${title}"/>
        </c:if>
        <c:set var="descr" value="${iz:clear_descr(iz:text_normalize(descr))}"/>
        <c:set var="title_formated" value="<b>${iz:html_quotes(title)}</b><br>${iz:html_quotes(descr)}"/>
        <c:set var="desc" value="${iz:strip_str_pxl(descr, ' ', 'Arial', 13, 980)}"/>
        <c:set var="desc" value='${iz:mark_words(desc, query)}'/>
        <h3 class="art-title">
          <a class="nod" data-toggle="tooltip" title="${title_formated}" href="${url}">${title}</a>
          <div style="display:none">${item.linkId}</div>
        </h3>
        <div class="art-news-sublink">
          <fmt:formatDate value="${iz:format_date(item.date)}" var="formattedDate"
                          type="date" pattern="yyyy/MM/dd"/>
          <span style="color: grey">${formattedDate}</span>
                    <span class="art-news-link-decor">
                    <c:out value="${iz:mark_part_words(domain, query)}" escapeXml="false"/>
                    </span>
        </div>
        <span class="am"></span>
        <p data-descmin="${desc}" data-descmax="${descr}">
          <c:out value="${desc}" escapeXml="false"/>
        </p>

        <div class="ptitles secFooter">
          <c:forEach var="subtitle" items="${snippet.subtitles}" varStatus='secStatus'>
            <c:set var="subtitle_formated"
                   value="<b>${iz:html_quotes(subtitle.title)}</b><br/>${iz:html_quotes(subtitle.description)}"/>
                <span>
                    <a data-toggle="sub-popup" role="popover"
                       data-title="${subtitle_formated}" href="${subtitle.url}"
                       data-img="${subtitle.imageId}">
                      <c:out value="${(iz:mark_words(iz:htmlentities(subtitle.cutedTitle), query))}"
                             escapeXml="false"/>
                    </a>
                    <c:if test="${!secStatus.last}"><span class="grvline">|</span></c:if>
                </span>
          </c:forEach>
        </div>
      </article>
    </section>
    <c:set var="sectionNumber" value="${sectionNumber + 1}"/>
  </c:if>
</c:forEach>
