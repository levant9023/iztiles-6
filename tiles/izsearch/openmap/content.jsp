<%-- 
  Document: header
  User:     eFanchik
  Date:     07.07.14
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/filters/openmap.css"/>

<%--
  <iframe id="mapframe" src="https://open.mapquest.com/?q=${query}"></iframe>
--%>
