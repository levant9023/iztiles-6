<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style>
  #adv-ad{
    display: none;
    margin-bottom: 35px;
  }
  #adv-ad .ad-title{
    overflow: hidden;
    height: 18px;
    text-overflow: ellipsis;
    white-space: nowrap;
    margin-bottom: 5px;
   /* margin-top: 25px;*/
  }
  #adv-ad .ad-title h3{
    margin: 0;
    padding: 0;
    overflow: inherit;
    font-weight: normal;
  }
  #adv-ad .ad-domain{
    color: green;
    overflow: hidden;
    height: 16px;
    text-overflow: ellipsis;
    white-space: nowrap;
    margin-bottom: 5px;
  }
  #adv-ad .ad-descr{
    overflow: hidden;
    height: 36px;
  }
  ._ak {
    border-bottom: 1px solid #ebebeb;
    border-top: 1px solid #ebebeb;
    padding: 10px 0;
  }
</style>

<div id="adv-ad"></div>