
<!-- Features -->
    <section class="features" id="features">
        <div class="container">
            <h2 class="text-center">Features</h2>

            <div class="row">
                <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-users"></span>
                            </div>
                        </div>

                        <div>
                            <h3>Social Media monitoring</h3>
                            <p>
                                Get instant access to mentions about your brand from social networks, influential publishers and your brand ambassadors. Take action in real time.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-newspaper-o"></span>
                            </div>
                        </div>

                        <div>
                            <h3>News, Blogs, Forums & More</h3>
                            <p>
                                Monitor live and get better understanding of who and what is saying about your brand in the News, Blogs, Forums, Video and other Web media.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-globe"></span>
                            </div>
                        </div>
                        <div>
                            <h3>Shopping, Sports, Travel, Science - over 50 sectors</h3>
                            <p>
                                Monitor brands and companies from over 50 sectors such as Tech, Health, Business, Food, Science and other.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-search"></span>
                            </div>
                        </div>

                        <div>
                            <h3>Excellent Features</h3>
                            <p>Filters by Date, Time, Language, Geo Location and other. SM Reach, 'Engagement', 'Buzz score', 'Global Influencers', 'Brand Ambassadors' and other parameters
                                important for your company.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-smile-o"></span>
                            </div>
                        </div>

                        <div>
                            <h3>Sentiment Analysis</h3>
                            <p>
                                Easily segment users' opinion about your brand or product by positive, negative, or neutral mentions. Identify statistically significant, 'Influencers' and
                                'Ambassadors' sentiments.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-calendar"></span>
                            </div>
                        </div>

                        <div>
                            <h3>Daily Digests, Custom & Real-Time Updates</h3>
                            <p>You can get daily digests, or set up custom times` updates or real time updates</p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-rss"></span>
                            </div>
                        </div>
                        <div>
                            <h3>Email or RSS Alerts</h3>
                            <p>You can set up alerts for your brands, products, competitors, keywords, authors, domains and even back-links.</p>
                        </div>
                    </div>
                </div>


                <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-pie-chart"></span>
                            </div>
                        </div>
                        <div>
                            <h3>Generate reports</h3>
                            <p>Generate PDF, Excel and text reports with detailed analysis, statistics and infografics. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Features -->

    <section id="owl" class="owl">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <div class="owl-carousel">
                        <div><img src="${baseURL}/resources/img/brands/owl/gillette_logo_cms.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/gucci_logo.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/Harvard-Logo.png"></div>

                        <div><img src="${baseURL}/resources/img/brands/owl/h_m.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/apple_logo4_2.png"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/samsunglogo.png"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/dominospizza.png"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/heineken4.png"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/costco.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/disney_logo_1.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/zara_3.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/dior1.png"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/atandt3.png"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/marlboro3.png"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/heinz2.png"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/mobil.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/polo23.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/mitsu.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/delta_airlines.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/cocacola.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/camel_1.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/general_electric.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/lg_group.jpg"></div>
                        <div><img src="${baseURL}/resources/img/brands/owl/geico_logo_cms.jpg"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>