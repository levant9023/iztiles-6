 <!-- Features -->
  <section id="features2" class="features">
    <div class="container">
      <h2 class="text-center">Why iZ Brands?</h2>

      <div class="row">
        <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-money"></span>
              </div>
            </div>

            <div>
              <h3>Less Expensive</h3>
              <p>
                Since iZ Brands utilizes its own search engine (izsearch.com) it can provide less expensive services and more flexible pricing plans, comparing to competitors that utilize third party search data.
              </p>
            </div>
          </div>
        </div>

        <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-binoculars"></span>
              </div>
            </div>

            <div>
              <h3>
                More Searches
              </h3>
              <p>
                iZ Brands utilizing its own search engine can provide
                more searches (Brands, Products, Keywords, etc.), filters and search parameters comparing to competitors.
              </p>
            </div>
          </div>
        </div>

        <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-database"></span>
              </div>
            </div>

            <div>
              <h3>
                More Data/Mentions
              </h3>
              <p>
                If you are a large enterprise, depending on Your needs, iZ Brands can provide more data/mentions, comparing to competitors that utilize third party
                search data.
              </p>
            </div>
          </div>
        </div>
        <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-clock-o"></span>
              </div>
            </div>

            <div>
              <h3>Specify Your Update Times</h3>
              <p>Specify several times of the day to get updates. Real-time updates are also available for premium users.</p>
            </div>
          </div>
        </div>

        <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-wrench"></span>
              </div>
            </div>

            <div>
              <h3>
                Customize for Your Case
              </h3>
              <p>Depending on your case, we can customize the data output, volume and freshness, search filters, sources and other specific parameters.</p>
            </div>
          </div>
        </div>

        <div class="feature-col col-lg-4 col-xs-12 col-sm-6">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-phone"></span>
              </div>
            </div>
            <div>
              <h3>
                Dedicated consultant
              </h3>
              <p>You will have a dedicated consultant and real time support on fixing the problems as well as discussing growth potential for your brand.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /Features -->
 <section id="owl" class="owl">
   <div class="container">
     <div class="row">
       <div class="col-lg-12 col-sm-12 text-center">
         <div class="owl-carousel">
           <div><img src="${baseURL}/resources/img/brands/owl/gillette_logo_cms.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/gucci_logo.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/Harvard-Logo.png"></div>

           <div><img src="${baseURL}/resources/img/brands/owl/h_m.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/apple_logo4_2.png"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/samsunglogo.png"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/dominospizza.png"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/heineken4.png"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/costco.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/disney_logo_1.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/zara_3.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/dior1.png"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/atandt3.png"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/marlboro3.png"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/heinz2.png"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/mobil.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/polo23.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/mitsu.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/delta_airlines.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/cocacola.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/camel_1.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/general_electric.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/lg_group.jpg"></div>
           <div><img src="${baseURL}/resources/img/brands/owl/geico_logo_cms.jpg"></div>
         </div>
       </div>
     </div>
   </div>
 </section>