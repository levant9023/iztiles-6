<%--
  User: efanchik
  Date: 7/27/15
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- #footer -->
<footer id="footer">
    <ul class="nav">
        <li><a href="${baseURL}/about.html">About</a></li>
        <li><a href="http://blog.izsearch.com">Blog</a></li>
        <li><a id="bbtn-api" href="${baseURL}/api.html">News API</a></li>
        <li><a id="bbtn-advertise" href="${baseURL}/advertise.html">iZ Ads</a></li>
        <li><a id="bbtn-izebra" href="${baseURL}/app.html">iZebra App</a></li>
        <!--<li><a id="bbtn-engine" data-toggle="modal" data-target="#btn_engine"><i class="btn-suscr glyphicon glyphicon-plus"></i>Add to Search</a></li>-->
        <li><a id="bbtn-homepage" href="/">Set as Home</a></li>
    </ul>
</footer>