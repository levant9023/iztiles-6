<%--
  User: efanchik
  Date: 6/8/15
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<!DOCTYPE HTML>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="meta_title" value="${requestScope.meta_title}" scope="request"/>

<html lang="<c:out value="${lang}"/>">
<head>
  <base href="${baseURL}/">
  <meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description"
        content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results.">
  <meta name="keywords"
        content="izsearch, izsearch home, izsearch home page, izsearch search, izsearch web, news, blog, forum, health, sports, travel, tech, entertainment, video, image">
  <title>iZSearch:&nbsp;<c:out value="${meta_title}" default=""/></title>
  <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
  <link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/custom.css"/>
  <%--<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/media.css"/>--%>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/bugreport/bugreport.css"/>



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquerypp.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/moment-locales.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/modules/js.cookie.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>
  <script type="text/javascript">
    var lang = "<c:out value='${lang}' default='en'/>";
  </script>
  <script type="text/javascript"
          data-main="${baseURL}/resources/js/production/mvc/controls/bugreport/app.js"
          src="${baseURL}/resources/js/production/require.js">
  </script>
  <script>
    (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-58665066-1', 'auto');
    ga('require', 'linkid', 'linkid.js');
    ga('send', 'pageview');
    setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
  </script>
</head>
<body>

<!-- wrapper -->
<div id="wrapper" class="container-fluid">
  <tiles:insertAttribute name="header"/>
  <section id="middle" class="row">
    <tiles:insertAttribute name="center"/>
  </section>
  <tiles:insertAttribute name="footer"/>
</div>
<!-- wrapper -->
</body>
</html>

