
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/images.css" />

<c:set var="image_list" value="${requestScope.images}" />
<c:if test="${not empty image_list}">
  <ul class="imagelist">
    <c:forEach var="elems" items="${image_list}" varStatus="status">
      <c:forEach  var="image" items="${elems.images}">
        <li>
          <c:url var="img_url" value="${image}"  />
          <a href="${img_url}" class="fancybox" rel="gallery">
            <img src="${img_url}" class="imagestyle"  alt="image"/>
          </a>
        </li>
      </c:forEach>
    </c:forEach>
  </ul>
</c:if>